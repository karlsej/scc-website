<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$course = '';
$campaignName = '';
$campaignData = ''; // to make this available to JS
$campaignSource = '';
$timestamp = '';

// 'context_title' is Name in Canvas settings. 'context_label' is course code and does not display... perhaps people are changing one and not the other?
if (isset($_POST['context_label'])) {
	// this means canvas is the source
	$course = $_POST['context_label'];
	include_once('testing.php');

	$campaignData = str_replace(' ', '+', $course);
	$campaignData = str_replace('/', '\/', $campaignData); // not sure this is needed
	// add campaign name and source for tracking in GA
	$campaignName = "ga('set', 'campaignName', '".$course. "'); "; 
	$campaignSource = "ga('set', 'campaignSource', 'canvas');";
	if(strpos($course, 'SCC') !== 0) { // when people have changed the course code in Canvas, do not allow textbook search to take place
		$campaignData = '';
	}
	$announceTest = '';
	if (strpos($course, 'Karlsen') !== false) {
		$campaignName = '';
		$announceTest = '<p>Class being used for this test page: ' . $course . '</p>';
	}

}


// if we want to personalize
//$myRecord = '';
//if (isset($_POST['lis_person_name_given'])) {
//	$myRecord = $_POST['lis_person_name_given'] . '&apos;s';
//}
?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="res/style.css?0408">
    <title>SCC Library</title>
<script>
	if ((window.location === window.parent.location) ||(document.cookie.indexOf('lrGAOptOut=y') > -1)) {
/* in case anyone views this outside of Canvas. Could change things so it's simply not viewable. Reevaluate later */
		window['ga-disable-UA-15285327-1'] = true;
	}
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-15285327-1', 'auto');
<?php
  echo $campaignName;
  echo $campaignSource;
  
 ?>
  ga('set', 'anonymizeIp', true);
  	ga('send', 'pageview');


</script>
   
</head>

<body data-course="<?php echo $campaignData; ?>">
<?php echo $announceTest; ?>
<h1>
    SCC Library
</h1>
<?php
if (strpos($_POST['roles'], 'Instructor') !== false) {
	if (!(isset($_COOKIE['instructor-banner']))) {
		echo '<div id="instructor-banner" style="display:none;"><p>Hello, SCC instructor! This page is here to help your students. <a href="https://answers.library.losrios.edu/scc/faq/336446" target="_blank">Read more about its features</a>.</p><button id="hide-instructor-banner" >Dismiss this message</button></div>';
	}

	
}
?>
<h2>
    Search
</h2>

<form action="https://caccl-lrccd.primo.exlibrisgroup.com/discovery/search" method="get" target="_blank" id="onesearch-form" aria-describedby="search-explanation">
    <label for="keywords">
        Enter keywords to search for books, articles, ebooks &amp; more
    </label>
    <p id="search-explanation">Use OneSearch to find online articles, ebooks, and video, plus books &amp; videos from library shelves</p>
<div id="input-container">
 <input type="hidden" name="search_scope" value="scc_everything" >
    <input type="hidden" name="tab" value="everything" >
    <input type="hidden" name="vid" value="01CACCL_LRCCD:scc" >
    <input type="hidden" name="query" value="any,contains,">
    <input type="text"  name="query" id="keywords" autocomplete="off" >
    <input type="submit" id="form-submit" value="Search" >
    </div>
    
</form>
<div class="two-col">
    <div class="col1">
	<div id="guides" style="display:none;">
		<h2>Your Research Guide</h2>
		<p><strong>This course has a library research guide!</strong></p>
		<p>
			<a id="guide-url">View it now.</a>
		</p>
		
	</div>
<h2>Essentials</h2>
    <ul id="essentials">

        <li>
            <a href="https://caccl-lrccd.primo.exlibrisgroup.com/discovery/search?vid=01CACCL_LRCCD:scc" id="onesearch-btn"  title="Find books, articles, ebooks and more" target="_blank">OneSearch</a>
        </li>
        <li>
            <a href="https://library.losrios.edu/databases/?college=scc" title="Browse collections of articles and other information resources" id="db-btn" target="_blank">Research Databases</a></li>
        <li>
            <a href="https://scc.losrios.edu/student-resources/library/research/pilot-tutorials" id="pilot-btn" title="Take our online information literacy tutorial" target="_blank">PIL<img id="compass"  src="res/compass.png" alt="O" width="22" height="22" >T</a>
        </li>
    </ul>

    
    </div>

    <div class="col2">
        <h2>Today&apos;s Library Hours</h2>
        <div id="todays-hours">
            
        </div>
        <div id="alt-hours-display">
            <h3>
                Fall &amp; Spring Semesters
            </h3>
            <ul>
                <li>Monday to Thursday: 9:00 am to 3:00 pm</li>
                <li>Friday to Sunday: closed</li>
              <!--  <li>Saturday: 9 am &ndash; 3 pm</li>
                <li>Sunday: closed</li-->>
                
            </ul>
            <p><a href="https://scc.losrios.edu/student-resources/library/library-daily-hours" id="hours-link" target="_blank">More about hours</a></p>
        </div>
        <h2>Contact</h2>
        <div id="chat-area">
								<div id="libchat_3ed10430124d950ef2b216a68e1b18ba"></div>
     
        <p>Phone: (916) 558-2461</p>
        </div>
    </div>

    </div>

    <div id="more-links">
        <h2>More from the Library</h2>
        <ul >
           <li><a href="https://scc.losrios.edu/student-resources/library/textbooks" target="_blank">Textbooks</a></li>
            <!-- <li><a href="https://www.scc.losrios.edu/student-resources/library/research/research-instruction" target="_blank">Library Workshops</a></li>-->
            <li><a href="https://library.losrios.edu/scc/research-appointments/" target="_blank">Research Appointments</a></li>
            <li><a href="http://researchguides.scc.losrios.edu/" target="_blank">Research Guides</a></li>
 
        </ul>
        <p>Find even more at the <a href="http://scc.losrios.edu/library/" target="_blank">Library website</a>!</p>
   
    </div>
				<div id="row-two">
									<div id="textbook-display"  style="display:none;" class="col1">
					<h2>Your Textbook</h2>
					<div id="tb-block">
						<?php
						$textbookFile = 'res/textbooks/' .$campaignData . '.php';
						if (file_exists($textbookFile)) {
							include_once($textbookFile);
						}
						?>
						
					</div>
						<p>While campus remains closed, most library reserves are available to be checked out for five days at a time. <a href="https://answers.library.losrios.edu/scc/faq/282373" target="_blank">Read more</a>.</p>
						<p>Not your textbook? Want to stop seeing textbooks? <button id="hide-textbooks" type="button">Hide</button></p>
				</div>
					
				</div>
				<footer>
					<p id="ga-in">The Library uses Google Analytics to assess and improve this page. Certain anonymous data regarding your use of this page is being sent to Google Analytics and is covered by <a href="https://www.google.com/policies/privacy/" target="_blank">Google&apos;s privacy policy</a>. <a href="//library.losrios.edu/resources/notes/google-analytics.php" target="_blank">Read more</a>. If you prefer, you may <button class="ga-but" id="gaOptOut">opt out</button>.</p>
					<p id="opted-out" style="display:none;">The Library uses Google Analytics to assess and improve this page. You have opted out. <button id="gaOptIn" class="ga-but">Opt in</button></p>
				</footer>
<div id="libguides-import" style="display:none;">
	<?php
	include_once('res/cache.php');
	?>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="res/jquery-ui.min.js"></script>

<script src="res/canvas-scripts.js?0826"></script>
<script src="https://answers.library.losrios.edu/load_chat.php?hash=3ed10430124d950ef2b216a68e1b18ba"></script>

</body>
</html>
