<?php
// show errors for debugging
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);


// source:  https://stackoverflow.com/a/5263017
$headPath = pathinfo(__FILE__)['dirname'];
$cache_file = $headPath . '/libguides/guide_list.php';
$subjID = '219718'; // changes every semester
$url = 'https://lgapi-us.libapps.com/widgets.php?site_id=289&widget_type=1&search_terms=&search_match=2&sort_by=name&list_format=1&drop_text=Select+a+Guide...&output_format=1&load_type=2&enable_description=0&enable_group_search_limit=0&enable_subject_search_limit=0&subject_ids%5B0%5D=' . $subjID . '&widget_embed_type=2&num_results=0&enable_more_results=0&window_target=2&config_id=1537200754513';
if (file_exists($cache_file) && (filemtime($cache_file) > (time() - 60 * 30 ))) {
   // Cache file is less than 30 minutes old. 
   // Don't bother refreshing, just use the file as-is.
   $file = file_get_contents($cache_file);
} else {
   // Our cache is out-of-date, so load the data from our remote server,
   // and also save it over our cache for next time.
   $file = file_get_contents($url);
   if ($file !== false) {
      file_put_contents($cache_file, $file, LOCK_EX);
   }
   else {
      $file = file_get_contents($cache_file);
   }
}
echo '<div style="display:none;">Cache file time: ' . date('F d Y H:i:s.', filemtime($cache_file)) . '</div>';
echo "\r\n";
echo $file;
?>