(function() {
	
// named functions
var getCookie = function(cname) {
	var name = cname + '=';
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
};
var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=losrios.edu;path=/";
};
var parseCourse = function(course) {

	var arr = course.split('+');
	// remove empty strings - happens sometimes
	var newArr = [];
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] !== '') {
			newArr.push(arr[i]);
		}
	}
	var college = newArr[0];
	var termArr = newArr[1].split('');
	var term = termArr[0] + termArr[1];
	var year = '20' + termArr[2] + termArr[3];
	var dept = newArr[2];
	num = newArr[3];
	if (num.indexOf('\/') > -1) {
		num = num.replace(/\/.*/, '');
	}
	var prof = newArr.pop();
	var courseCode = newArr.pop();
	if (courseCode.indexOf('/') > -1) {
		courseCode = courseCode.replace(/\/.*/, '');
	}
	return ({
		'college': college,
		'term': term,
		'year': year,
		'department': dept,
		'courseNumber': num,
		'professor': prof,
		'courseID': courseCode

	});
};
var findGuide = function (obj){
	// looks for Libguide with label matching course string
	var str = obj.department + ' ' + obj.courseNumber + ' (' + obj.professor + ')';
	str = str.toLowerCase();
	console.log(str);
		var lgURL = '';
		$('#libguides-import').find('a').each(function() {
			var l = $(this).text().toLowerCase();
			if (l.indexOf(str) > -1) {
				lgURL = $(this).attr('href');
				return lgURL;
			}
			});
		return lgURL; // returns empty string if no match

};
var autoComp = function(el) { // get EBSCO autocomplete
	el.autocomplete({
		source: function(request, response) {
			$.getJSON({
				url: 'https://widgets.ebscohost.com/prod/simplekey/autocomplete/autocomp.php',
				data: {
					q: encodeURIComponent(request.term)
				}
			})
            .done(function(data) {
				data = JSON.parse(data); // turn response into JSON object
				var array = $.map(data, function(value) // convert to array
					{
						return [value];
					});
				var output = []; // create array of suggested terms, which is what jQuery UI expects
				var arrayVals = array[2]; // this is where the objects are in EBSCO's response
				for (var i = 0; i < arrayVals.length; i++) {
					output.push(arrayVals[i].term); // push suggestions into the array
					response(output); // sends the array to jQuery ui - needs to be done inside the loop so it updates as you type
				}
			})
            .fail(function(a, b, c) {
				ga('send', 'event', 'eds autosuggest', 'error', c);
			});
		},
		//   minLength: 2,
		select: function(event, ui) {
			if (ui.item) {
				el.val(ui.item.value);
			}
			$('#onesearch-form').submit();
		}
	});
};

var showToday = function() // show current days hours using libcal
{
	$.getJSON('https://api3.libcal.com/api_hours_today.php?iid=462&lid=4504&format=json&callback=?', function(h) {
		var output;
		var a = h.locations[0].rendered;
		if ((a.indexOf('Recess') > -1) || (a.indexOf('Closed') > -1)) {
			output = 'Closed';
		} else {
			if (h.locations[0].times.note) {
				a.replace(/pm.*/, 'pm');
			}
			a = fixFormatting(a);
			output = a;
		}
		$('<p />').html(output + ' &#40;<a href="https://scc.losrios.edu/student-resources/library/library-daily-hours" target="_blank">more about hours</a>&#41;').appendTo($('#todays-hours'));
	});
};

var fixFormatting = function(str) { // comply with SCC styleguide
	str = str.replace(/am/g, ' am').replace(/pm/g, ' pm').replace('C ampus', 'Campus').replace(/\-/g, ' &ndash;');
	return str;
};



// text that will be placed below icon
var online = $('#chat-online');
var offline = $('#chat-offline');
var container = $('.libchat-block');
var askPage = false;
if (location.pathname.indexOf('services/ask-librarian') > -1) {
	askPage = true;
	container.appendTo($('#chat-on-page'));
}
// alt attributes set in LibChat widget

chatID = '12136'; // widget ID
var showTextbooks = function() {
	if (getCookie('hideTextbookBox_' + textbookEDSQuery) !== 'true') {
		var items = $('.textbook-bk');
	if (items.length) {
		var area = $('#textbook-display');
		if (items.length > 1) {
			area.find('h2').html('Your Textbooks');
		}
		area.show();
		$('.textbook-link.no-cover').each(function() {
			var el = $(this);
			if (el.data('isbn') !== '') {
				var isbn = el.data('isbn');
				var title = el.html();
				var img = '<img id="img-' + isbn + '" style="position:absolute; left:-999999px;" src="//contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&amp;Password=ebsco-test&amp;Return=1&amp;Type=M&amp;Value=' + isbn + '" alt="' + title + '">';
				$('body').append(img);
				setTimeout(function() {
					var loadedImg = $('#img-' + isbn);
					if (loadedImg.height() > 10) {
						el.html('').removeClass('no-cover');
						loadedImg.removeAttr('style').appendTo(el);
					}
				}, 2000);
			}	
			
		});
		$('#more-links').addClass('col2').appendTo($('#row-two'));
		
	}
	$('#hide-textbooks').button({ // use jQuery UI button styles
			classes: {
				"ui-button": "ui-state-default"
			}
		})
		.on('mouseover focus', function() {
			$('#tb-block').css('visibility', 'hidden');

		})
		.on('mouseout blur', function() {
			$('#tb-block').css('visibility', 'visible');

		})

		.on('click', function() {
			setCookie('hideTextbookBox_' + textbookEDSQuery, 'true', 150);
			$('#tb-block').fadeOut('slow', function() {
				$('#textbook-display').html('<p id="no-textbooks" role="alert" style="display:none;">The textbook area will no longer appear when you access this course on this computer.</p>');
				$('#no-textbooks').fadeIn();
				ga('send', 'event', 'textbook links', 'hide');
			});

		});
	$('.textbook-link').on('click', function() {
		ga('send', 'event', 'textbook links', 'click');
	});
	if ($('.ebook-badge').length) {
		$('.ebook-badge').each(function() {
			var id = $(this).attr('id');
			$(this).parents('.textbook-link').attr('aria-describedby', id);

		});
	}
	}
};
var storeCache = function(output) {
	var o = output || '';
	$.post('res/textbook-cache.php', {
			filename: $('body').data('course'),
			data: o,
			time: new Date()
		})
	.done(function(d) {
		$('#tb-block').html(d);
		if (d === 'error') { // php file echoes this if file_put_contents fails
			ga('send', 'event', 'textbook cache', 'error', 'php file' );
		}
		showTextbooks();
		})
		.fail(function(a, b, c) {
			ga('send', 'event', 'textbook cache', 'error', c);
		});
};
var getTextbooks = function(q) {
	
	var primoURL = 'https://api-na.hosted.exlibrisgroup.com/primo/v1/search?vid=01CACCL_LRCCD%3Ascc&tab=textbooks&scope=scc_textbooks&q=any%2Ccontains%2C' + encodeURIComponent(q) + '&lang=eng&offset=0&limit=10&sort=rank&pcAvailability=false&getMore=0&conVoc=false&inst=01CACCL_LRCCD&skipDelivery=true&disableSplitFacets=true&apikey=l8xx00444a6f9cf74a888db68745a8544b3c';

	
	$.getJSON(primoURL)
		.done(function(data) {
			console.log(data);
			window.data = data;
			var items = data.info.total;
			if (items) {
				if (items === 0) {
					console.log('zero items');
					storeCache();
					return false;
				}
				console.log('still running');
				var list = false;
				var output = '';
				if (items > 1) {
					list = true;
					output += '<ul>';
				}
				else {
					output += '<div>';
				}
				var docs = data.docs;
				var noCover = 'no-cover';
				for (var i = 0; i < docs.length; i++) {
					if (list) {
						output += '<li class="textbook-bk ' + noCover + '">';
					} else {
						output += '<div class="textbook-bk ' + noCover + '">';
					}
					recordId = docs[i].pnx.control.recordid[0] || '';

					if (recordId !== '') {
						var itemISBN = '';
						var dataISBN = '';
						var isbns = docs[i].pnx.addata.isbn || [];
						for (var b = 0; b < isbns.length; b++) {
							if (/^978\d{9}[x\d]$/.test(isbns[b]) === true) {
								console.log('13 digit');
								itemISBN = isbns[b];
								break;
							}
							else if (/^\d{10}$/.test(isbns[b]) === true) {
								console.log('10-digit');
								itemISBN = isbns[b];
							}
						}
						if (itemISBN !== '') {
							dataISBN = ' data-isbn="' + itemISBN + '" ';
						}
						var linkSt = 'https://caccl-lrccd.primo.exlibrisgroup.com/discovery/fulldisplay?docid=' + recordId + '&amp;context=L&vid=01CACCL_LRCCD:scc&amp;search_scope=scc_textbooks&amp;tab=textbooks';
						var title = docs[i].pnx.display.title[0];
						output += '<a ' + dataISBN + ' class="textbook-link ' + noCover + '"  href="' + linkSt + '" target="_blank" title="Go to OneSearch record for &quot;' + title + '&quot;">';
						output += title;
						output += '</a>';
						if (list) {
							output += '</li>';
						}
						else {
							output +='</div>';
						}
					}
					}
					if (list) {
						output += '</ul>';
					}
					else {
						output += '</div>';
					}
					storeCache(output);
					
				
			}
			else {
				storeCache();
			}
		})
		.fail(function(a, b, c) {
			console.log('error: ' + c);
			ga('send', 'event', 'primo api', 'error', c);
		});



};

$('#form-submit').button({ // use jQuery UI button styles
	classes: {
		"ui-button": "ui-state-default"
	}
})
.on('mouseover', function() {
	$(this).addClass('ui-state-hover');
})
.on('mouseout', function() {
	$(this).removeClass('ui-state-hover');
});
 

	if ($('body').width() > 400) {
	// tooltips for essentials and chat
	$('#essentials a').tooltip( {
		position: {
			my: 'right-15 bottom'
		},
        classes: {
			'ui-tooltip': 'ui-state-highlight'
		}
	}
	);
	$('#chat-area a').tooltip( {
		position: {
			at: 'right+30 top-30'
		},
        classes: {
			'ui-tooltip': 'ui-state-highlight'
		}
	}
	);
}
	


    // autocomplete using EBSCOs system plus jQuery UI
    autoComp($('#keywords'));

    showToday();
    
    setTimeout(function ()
    { // if libcal fails. with jsonp you have to do it this way
      if (!($('#todays-hours p').length))
      {
        $('#alt-hours-display').show();
        ga('send', 'event', 'libcal hours', 'error');
      }
    }, 10000);



	if (getCookie('lrGAOptOut') === 'y') {
		$('#ga-in').hide();
		$('#opted-out').show();
	}



	(function() {
		var course = $('body').data('course');
		if (course !== '') {
			var data = parseCourse(course);
			courseStr = data.department + ' ' + data.courseNumber + ' ' + data.professor;
			if (data.courseID !== '') {
				if (/^\d{5}$/.test(data.courseID) === true) {
					window.textbookEDSQuery = data.courseID;
				}
				else {
					window.textbookEDSQuery = courseStr;
				}
			}
				
			else {
				window.textbookEDSQuery = courseStr;
			}
		
			showTextbooks();
			// check for guide
			var guideURL = findGuide(data);
			console.log(guideURL);
			if (guideURL !== '')
			{
				var connector = '?';
				if (guideURL.indexOf('?') > -1)
				{
					connector = '&';
				}

				var target = guideURL + connector + 'utm_source=canvas';
				$('#guide-url').attr('href', target);
				$('#guides').show();
			}
			var addedLinks = [
			{
				dept: 'ENGWR',
				links: [
				{
					label: 'MLA Citation Guide',
					url: 'https://researchguides.scc.losrios.edu/mla-style-guide'
				}]
			},
			{
				dept: 'NURSE',
				links: [
				{
					label: 'Ovid Journals',
					url: 'https://ezproxy.losrios.edu/login?url=http://ovidsp.ovid.com/ovidweb.cgi?T=JS&amp;CSC=y&amp;PAGE=main&amp;NEWS=y&amp;DBC=y&amp;D=yrovft'

				}]
			},
			{
				dept: 'PTA',
				links: [
				{
					label: 'Rehabilitation Reference Center',
					url: 'https://ezproxy.losrios.edu/login?url=https://search.ebscohost.com/login.aspx?authtype=ip%2Cuid&amp;profile=rrc'
				}]

			}];
			for (var i = 0; i < addedLinks.length; i++)
			{
				if (addedLinks[i].dept === data.department)
				{
					var links = '';
					var linksArr = addedLinks[i].links;
					for (var j = 0; j < linksArr.length; j++)
					{
						links += '<li><a href="' + linksArr[j].url + '" target="_blank">' + linksArr[j].label + '</a></li>';
					}
					$('#more-links ul').prepend(links);

				}

			}
			var staleCache = false;
			if ($('#textbook-cache').length)
			{
				var cacheTime = new Date($('#textbook-cache').data('timestamp'));
				var maxAge = 6 * 60 * 60 * 1000; // 6 hours
				if ((new Date()) - cacheTime > maxAge)
				{ // see if current block is older than max age
					staleCache = true;
				}

			}
			else
			{
				staleCache = true;
			}
			if (staleCache !== false)
			{ // refresh cache if it's old
				getTextbooks(textbookEDSQuery);
			}
		}
		
	}());


	$('#gaOptOut').on('click', function() {
		setCookie('lrGAOptOut', 'y', 120);
		$('#ga-in').fadeOut();
		$('#opted-out').fadeIn();	
	});
	$('#gaOptIn').on('click', function() {
		setCookie('lrGAOptOut', 'n', 1);
		$('#opted-out').fadeOut();
		$('#ga-in').fadeIn();	
	});

		var instBan = $('#instructor-banner');
		if (instBan.length) // php checks for cookie
		{
			setTimeout(function()
			{
				instBan.slideDown();
				ga('send', 'event', 'instructor banner', 'show');
				$('#hide-instructor-banner').button().on('click', function()
				{
					setCookie('instructor-banner', 'hide', 90);
					ga('send', 'event', 'instructor banner', 'hide');
					instBan.slideUp();
				});
			}, 1000);
		}

	
	

	    // remaining analytics events

	$('#onesearch-form').on('submit', function() {
		var a = $('#keywords').text();
		if (a !== '') {
			ga('send', 'event', 'canvas search box', 'submit', a);
		}
		});
	$('#essentials a').on('click', function ()
    {
      var a = $(this).text();
/*	  if (a.indexOf('Record') > -1) { // because of goofy personalization we're trying
		a = 'My Library Record';
	  } */
      ga('send', 'event', 'essentials', 'click', a);
    });
    $('#more-links a, #hours-link').on('click', function ()
    {
      var a = $(this).text();
      ga('send', 'event', 'canvas page links', 'click', a);
    });
	$('#onesearch-form').on('submit', function(e) {
		e.preventDefault();
		var urlBase = 'https://caccl-lrccd.primo.exlibrisgroup.com/discovery/search?vid=01CACCL_LRCCD:scc';
		var a = $('#keywords').val();
		if (a === '') {
			window.open(urlBase, 'newWin');
		}
		else {
			window.open(urlBase + '&query=any,contains,' + a + '&search_scope=scc_everything&tab=everything', 'newWin');
		}
		ga('send', 'event', 'canvas search box', 'submit', a);
	});

}());

