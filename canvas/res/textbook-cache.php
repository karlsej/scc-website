<?php
$data = '';
$time = '';
$filename = '';
if (isset($_POST['data'])) {
	$data = $_POST['data'];
}
if (isset($_POST['time'])) {
	$time = $_POST['time'];
}
if (isset($_POST['filename'])) {
	$filename = $_POST['filename'];
}
if (($data === '') && ($time === '') && ($filename === '')) {
	echo 'error';
}
else {
	$output = '<div id="textbook-cache" data-timestamp="' . $time . '"></div>';
	if ($data !== '') {
		$output .= $data;
}

$file = file_put_contents(__DIR__ . '/textbooks/' . $filename . '.php', $output, LOCK_EX);
echo $output;
}
exit;
?>