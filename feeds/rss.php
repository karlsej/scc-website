<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
$baseURL = 'https://caccl-lrccd.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01CACCL_LRCCD:scc&docid=alma';
$proxy = 'https://ezproxy.losrios.edu/login?url=';
header('Content-Type: text/xml; charset=utf-8');
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('America/Los_Angeles');
function returnSnippet($str, $len) {
	$newStr = $str;
	$arr = explode(' ', $str);
	if (count($arr) > $len) {
		$newArr = array_slice($arr, 0, $len);
		$newStr = implode(' ', $newArr) . '...';
	}
	return $newStr;
	
}
$listType = $_GET['list'];
$fileName = $listType . ".txt";
$feedName = str_replace('-', ' ', $listType);
$f = fopen($fileName, "r");
$update = date ("D, d M Y H:i:s", (filemtime($fileName)) - (9 * 60 * 60)) . ' PST'; // gets timestamp from tsv file. I don't think this accounts for time zone
/* echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>"; */
$matType = array(
	'Book' => 'bk',
	'None' => 'bk',
	'Ebook' => 'ebk',
	'Streaming Video' => 'vid',
	'Other Visual Material' => 'vid',
	'DVD' => 'vid'
);
echo "<rss version=\"2.0\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n";
echo "<channel>\n";
echo "<title>" .$feedName . " feed</title>\n";
echo "<description>New acquisitions from the Sacramento City College Library</description>\n";
echo "<language>en-us</language>\n";
echo "<link>http://www.scc.losrios.edu/library/new-materials/</link>\n";
// echo "<atom:link href=\"http://dallas.example.com/rss.xml\" rel=\"self\" type=\"application/rss+xml\" />\n";
echo "<webMaster>karlsej@scc.losrios.edu (Jeff Karlsen)</webMaster>\n";
echo "<lastBuildDate>" . $update ."</lastBuildDate>\n";
// adapted from http://stackoverflow.com/a/28478394/1903000
while (($line = fgetcsv($f,20000,"\r")) !== false) {
        $row = $line[0];    // We need to get the actual row (it is the first element in a 1-element array)
        $cells = explode("\t",$row); // tab is used as delimiter. Control character 9 in Sierra export
        $title= htmlspecialchars($cells[0]);
        $title = str_replace(' :', ':', $title);
				$title = str_replace(' ;', ':', $title);
        $title = str_replace('[videorecording]', '', $title);
		$title = returnSnippet($title, 20);
        $dateCat = htmlspecialchars($cells[1]);
        $date = strtotime($dateCat);
        $newdate = DateTime::createFromFormat('Y-m-d', $dateCat);
        $prettyDate = $newdate->format('l, F j'); // this is for content part of feed
        $idDate = $newdate->format('Y-m-d');
        $itemDate = $newdate->format('D, d M Y H:i:s') . ' PST';
        $recordNo = htmlspecialchars($cells[2]);
        $summary = htmlspecialchars(html_entity_decode($cells[3], ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES, 'UTF-8'); // reference: http://php.net/manual/en/function.html-entity-decode.php#93378
		//$summary = str_replace('"', '', $summary);
		//$summary = returnSnippet($summary, 30);
        //$summary = str_replace('"', '', $summary);
//	$summary = htmlentities($summary);
        $isbn = htmlspecialchars($cells[4]);
        $singleIB = preg_match('(\d{13})', $isbn, $isbns); // 13-digit works better for openlibrary covers
        if (!empty($isbns)) {
		$isbn = $isbns[0];
	}
	else {
		$isbn = '';
				
	}
	$year = htmlspecialchars($cells[5]);
	$link = htmlspecialchars($cells[6]);
	$mat = $cells[7];
	$matClass = '';
	if ($mat !== '') {
		$matClass = $matType[$mat];
	}

//        $year = preg_replace('/[\.|c|\[|\]]/', '', $year);
       $primoURL = $baseURL . $recordNo;

if (empty($link)) {
	$link = htmlspecialchars($primoURL);
}
	if (preg_match('/caccl-lrccd|kanopy|losrios\.edu/', $link) === 0) {
		$link = str_replace('%26', '&amp;', $link);
		$link = $proxy . $link;
	}
//        $summary = str_replace('&quot;', '', $summary);
				$imgSrc = '';
				if ($isbn !== '') {
					 $imgSrc = "&lt;a href=&quot;" .$link . "&quot;&gt;&lt;img class=&quot;jacket&quot; alt=&quot;item cover&quot; src=&quot;https://covers.openlibrary.org/b/isbn/" .$isbn ."-M.jpg&quot; /&gt;&lt;/a&gt;";
				}
 //       $summary = str_replace('"""', '"', $summary);
 //       $summary = str_replace('""', '"', $summary);
        //if (strlen($summary) > 700) {
        //    $summary = trim(substr($summary, 0, 700)) . '... ';
        //    }

        echo "<item> \n";

        echo "<title>" . $title ." (" .$year .")</title> \n";
 //       echo "<id>tag:scc.losrios.edu," .$idDate . ":/library/feeds/perm.php?record=" .$recordNo ."</id> \n";
        echo "<pubDate>" .$itemDate ."</pubDate>\n";
        // content type needs to be xhtml in order not to have to escape all html characters...
        echo "<description>" . $imgSrc . "&lt;p class=&quot;summary " . $matClass ."&quot;&gt;&lt;span class=&quot;updated&quot;&gt;(Added " .$prettyDate . ")&lt;/span&gt; " . $summary ."&lt;/p&gt;</description>\n";
        echo "<link>" .$link . "</link>\n";
	echo "<guid>" . htmlspecialchars($primoURL) . "</guid>";
        echo "</item>\n";
}

fclose($f);
echo "</channel>\n";
echo "</rss>"
?>