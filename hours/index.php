<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
    <!-- Please note: The following styles are optional, feel free to modify! //-->
<style>
    body {font-family:Arial, Verdana, sans-serif;}
    .sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}

.ui-dialog {
	background: #FDE373;
	padding: 0;
}

#s-lc-mhw462 tbody {
	background: #fff;
}

.s-lc-mhw-tb {
	margin: auto; border-collapse:collapse;
}

.s-lc-mhw-header {
	padding-bottom: 4px;
}

.s-lc-mhw-loc-name {
	display: none;
}

.s-lc-mhw-header-date {
	text-transform: uppercase;
	font-size: 1.4em;
	margin-left: 1em;
}

.s-lc-mhw-days {
	background: #8A0028;
	color: #fff;
	text-transform: uppercase;
}

.s-lc-mhw-days td,
.s-lc-mhw-days th {
	padding: 8px;
	border-right: 1px solid #fff;
}

.s-lc-mhw-days th:last-of-type {
	border-right: none;
}

.s-lc-mhw-day {
	border: 1px solid #ccc;
	padding: 4px 10px;
	min-width: 100px;
	max-width: 260px;
	height: 70px;
    position:relative;
}

.s-lc-mhw-day:first-child {
	min-width: initial;
}

.s-lc-mhw-day-l {
	font-weight: bold;
	margin-bottom: 4px;
    position:absolute;
    top:2px;
    left:2px;
}

.s-lc-mhw-loc {
	font-size: .9em;
	padding: 0 4px 0 1em;
}

.s-lc-note {
	display: block;
	font-style: italic;
}

.lc_today {
	outline: 4px solid #fde373;
	outline-offset: -4px;
}

@media(min-width:767px) {
	.hours-caption {
		min-width: 20em;
	}
}
</style>
</head>
<body>
    <div id="hours-table">
        <div id="s_lc_mhw_462_4504"></div> 
    </div>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://scc.losrios.libcal.com/js/hours_month.js?002"></script> 

<script>
$(function(){ 
var s_lc_mhw_462_4504 = new $.LibCalHoursCal($('#s_lc_mhw_462_4504'), {
	iid: 462,
	lid: 4504,
	months: 6,
	systemTime: true,
    show_past: 1
});

var wait = setInterval(function() {
    if ($('.s-lc-mhw-tb').length) {
		clearInterval(wait);
	}
    $('.s-lc-time').each(function() {
    $(this).html($(this).html().replace('am', ' am').replace('pm', ' pm'));
    
    });
    }, 100);



});
</script> 


</body>
</html>
