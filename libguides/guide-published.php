<?php

$http_origin = $_SERVER["HTTP_ORIGIN"];
 if ((strpos($http_origin, "libapps") > -1) || (strpos($http_origin, "losrios.edu") > -1))
{  
    header("Access-Control-Allow-Origin: " . $http_origin);
    header("Access-Control-Allow-Methods: GET, PUT, POST");
    header("Access-Control-Max-Age: 1000");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
  }
  $url = '';
  if (isset($_POST['link'])) {
    
    $url = $_POST['link'];
  }
  $title = '';
    if (isset($_POST['title'])) {
    
    $title = $_POST['title'];
  }
  if ($url !== '') {
    $output = 'New guide published: '. $url;
    $output .= "\r\n";
    $output .= $title;
    
    $to      = 'KarlseJ@scc.losrios.edu, TerchoK@scc.losrios.edu';
$subject = 'New Libguide published';
$message = $output;
$headers = 'From: no-reply@library.losrios.edu' . "\r\n" .  'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
    
  }
?>
