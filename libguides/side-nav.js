// cookies
function setCookie(cname, cvalue, exdays)
{
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + '; ' + expires + '; path=/;domain=losrios.edu';
}

function getCookie(cname)
{
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++)
  {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
  }
  return '';
}

function checkCookie() {
  if (location.hostname.indexOf('scc.losrios.edu') > -1) {
	var newWins = getCookie('windowPref');
  console.log(newWins);
	$('.s-lib-box-content a, .s-lib-box-content form').each(function() {
		var a = $(this);
		//   console.log(a.attr('href'));
		if (newWins === 'no') {
      console.log('same window');
			$('#target-setting').prop('checked', false);
			if (a.attr('href') !== undefined) {
				a.each(function() {
					if (!($(this).hasClass('toc'))) {
						$(this).attr('target', '_self');
					}
				});
			} /* else {
        console.log('new window');
				$('#target-setting').prop('checked', true);
				a.attr('target', '_blank');
			} */
		} else {
			$('#target-setting').prop('checked', true);
      if (a.attr('href').indexOf('researchguides.scc.losrios.edu') === -1) {
				a.attr('target', '_blank');
      }
		}
	});
}
}

(function ()
{
  if (window !== window.parent) {
    
    $('#header, #s-scc-tabs-container, #links-pref, #s-lib-footer-public').hide();
    $('#s-lg-guide-header-info').prepend('<p style="position:absolute; right:4px;"><a href="' + location.href + '" target="_blank">Open this guide in a new window</a></p>');
  }
  var profile = $('.s-lib-profile-container');
  if (profile.length) {
    var profileBox = profile.parents('div[id^="s-lg-box-wrapper"]');
    profileBox.addClass('profile-box');
    if ($(window).width() > 994) {
    
    profileBox.insertAfter('#s-lg-guide-tabs');
  }
  else {
    profileBox.appendTo('#s-lg-guide-main .row');
  }
  }

  $('a[href^="#s-lib-ctab-"]').on('click', function() {
    window.history.replaceState(null,null, location.pathname + location.search + $(this).attr('href'));
    if ($(window).width() < 994) {
       var target = $(this).attr('href');
    console.log('target is ' + target);
    console.log($(target));
      console.log($(target).offset().top);
      setTimeout(function() {
         $('html, body').animate(
      {
        scrollTop: $(target).offset().top
      });
        }, 80);
   
    }
   
  });

  if ($(window).width() < 994)
  {

    var label = $('#scc-side-nav-label');
    label.html(label.html().replace(/$/, '+'));
    label.on('click', function ()
    {
        var collapsible = $(this).next('ul');
        collapsible.collapse('toggle');
collapsible.on('shown.bs.collapse', function() {
    label.html(label.html().replace('+', '-'));
    });
collapsible.on('hidden.bs.collapse', function() {
    label.html(label.html().replace('-', '+'));
    });
      
    });
    label.click();
    var libMenu = $('#s-scc-guide-tabs');
    libMenu.find('ul:first-of-type').addClass('collapse');
    libMenu.prepend('<div id="lib-menu-nav-header"><span class="hamburger-icon"></span>Library Menu +</div>').attr('data-toggle', 'collapse-in').on('click', function() {
        var collapsible = $(this).find('.nav-tabs');
        collapsible.collapse('toggle');
        collapsible.on('shown.bs.collapse', function() {
    var label = $('#lib-menu-nav-header');
    label.html(label.html().replace('+', '-'));
    });
        
collapsible.on('hidden.bs.collapse', function() {
    var label = $('#lib-guide-nav-header');
    label.html(label.html().replace('-', '+'));
    });
        
        });
    $('.dropdown a').removeAttr('data-hover').attr({'data-toggle': 'dropdown', 'data-target': '#'});
    libMenu.find('.top').dropdown();
    libMenu.find('.s-lg-subtab-ul a').removeAttr('data-toggle data-target');


  }
  if (location.hash !=='') {
    setTimeout(function() {
    if ($('.ui-tabs').length) {
      var a = location.hash;
      $(a).click();
    }}, 500);
  }
  
})();



jQuery(document).ready(function ($)
{
  $('#target-setting').on('click', function ()
  {
    var a = $(this);
    var b = $('.s-lib-box-content a, .s-lib-box-content form');
    if (a.is(':checked'))
    {
      setCookie('windowPref', 'yes', 30);
      b.each(function() {
      if (!($(this).hasClass('toc'))) {
      $(this).attr('target', '_blank');
      }
      });
    }
    else
    {
      setCookie('windowPref', 'no', 1);
      b.removeAttr('target');
    }
  });
  var checkLinks = setInterval(function ()
  {
    if ($('.s-lib-box-content a').length)
    {
      clearInterval(checkLinks);
      checkCookie();
    }
  }, 200);
setTimeout(function() {
  if ($('.s-lg-rss-list').length) {
    checkCookie();
  }
  },1000);



}

);


