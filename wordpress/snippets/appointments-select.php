<?php
$weekendNote = '';
if (isset($_GET['weekend'])) {
	$weekendNote = '<p id="weekend" role="alert"> On Friday &amp; Saturday, please do not make an appontment for the coming Monday. If you do, the appointment will be canceled.</p>' . "\r\n"; 
}
$options = array(
	array(
		'label' => 'Main Campus',
		'id' => 'LEdn8eYjkyMkwFLoGGSk'
	),
	array(
		'label' => 'Davis Center',
		'id' => 'nG82YTKFCUEWHdpkAQHR'
	),
	array(
		'label' => 'West Sacramento Center',
		'id' => '6nfUZSZw4Gfv3v69CYys'
	)
);

$list = '<ul id="select">';
for ($i=0; $i < count($options); $i++) {
	$list .= '<li>';
	$list .= "\r\n";
	$list .= '<a href="https://classic.youcanbook.me/v2/jsps/index.jsp?cal=' . $options[$i]['id'] . '">' . $options[$i]['label'] . ' Appointments</a>';
	$list .= "\r\n";
	$list .= '</li>';
	$list .= "\r\n";
	
}
$list .= '</ul>';
$list .= "\r\n";


?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Select a Location</title>
	<style>
		body {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 90%;
			max-width: 800px;
		}
		.access-hide {position:absolute; left:-999999px;}

		h1,
		h2 {
			text-transform: uppercase;
			font-weight: 100;
		}

		h1 {
			color: #8A0028;
		}
		h2 {
			color:#4d4d4d;
			margin:30px 0 0;
			border-bottom:1px solid #E4E4E4;
		}

		ul {
			padding-left: 1em;
		}

		li {
			list-style-type: none;
		}
		a, a:link {color: #8A0028; text-decoration:underline;}
		a:focus {outline: thin dotted;}
		a:hover, a:focus {color: #4D567A;}

		#select a {
			display: inline-block;
			padding: 12px 12px 8px 12px;
			margin: 4px 2%;
			background: #FFCE00;
			font-size: 1.3em;
			border-radius: 4px;
			text-transform: none;
			font-family: "Helvetica Neue", Arial, sans-serif;
			font-weight: normal;
			text-decoration: none;
			color: #8A0028;
			text-decoration: none;
		}

		#select a:link {
			color: #8A0028;
		}

		#select a:hover,
		#select a:focus {
			color: #4d4d4d;
			background: #FDE373;
			text-decoration: none;
		}

		#select a:active,
		#select a:active {
			color: #8A0028;
		}

		#loader {
			display: none;
			margin: -200px auto 0 auto;
		}
		#weekend {
			font-weight:bold;
		}
	</style>

</head>

<body>
	<h1 class="access-hide">Select an Appointment</h1>
	<h2>About Research Appointments</h2>
	<p>Make a 30-minute appointment with an experienced reference librarian to get help selecting sources, using research and exploring your topic. <a target="_top" href="//www.scc.losrios.edu/library/services/research-appointments">More info</a></p>
	<h2>Select a Location</h2>
	<?php
		echo $weekendNote;
		echo $list;
	?>
	<h2>Note to instructors</h2>
	<p>The Research Appointment service should not be assigned for extra credit or other &quot;points&quot;; students will not be given proof that they used it.</p>
<p>If your entire class needs assistance, please arrange for <a target="_top" href="https://www.scc.losrios.edu/library/faculty-services/orientation-request/">group Library instruction</a>.</p>
		<img id="loader" alt="loading" src="//www.library.losrios.edu/resources/databases/loader.gif">
		<script>
			function showLoader() {
				document.getElementById('loader').style.display = 'block';
			}
			var a = document.getElementsByTagName('a');
			for (var i = 0; i < a.length; i++) {
				a[i].addEventListener('click', showLoader);

			}
		</script>
</body>

</html>