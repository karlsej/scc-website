_Lib.fn.endOfWeek = function() {
	var d = new Date();
	var n = d.getDay();
	if ((n === 5) || (n === 6)) {
		return true;
	} else {
		return false;
	}
};
(function() {
	var $ = jQuery;
	if (location.pathname.indexOf('services/research-appointments') > -1) {
		var block = $('#select-appointment');
		if (block.length) {
			block.appendTo($('#sign-up-now'))
				.html(block.html().replace('Make a Research Appointment', 'Sign up now'));
			block.addClass('ctaButton');
		}		
	}
	else if (location.pathname.indexOf('/group-study-rooms') > -1) {
		$('body').append('<p id="sc-gsr" style="display:none;"></p>');
		$('#sc-gsr').append($('#select-appointment'));
		$('#lib-cal ul').remove();
		$('#sc-gsr').appendTo($('#lib-cal')).show();
	}
	var weekend = '';
	if (_Lib.fn.endOfWeek()) {
		weekend = '<p id="weekend" role="alert"><strong>On Friday &amp; Saturday, please do not make an appointment for the coming Monday. If you do, the appointment will be canceled.</strong></p>';
	}

	$('#select-appointment').on('click', function(e) {
		e.preventDefault();
		var h = window.innerHeight * 0.95;
		var w = window.innerWidth * 0.9;
		if (w > 800) {
			w = 800;
		}
		var url = $(this).attr('href');
		//var directory = 'wordpress/widgets';
		//directory = 'bitbucket/scc-website/wordpress/snippets'; // for testing
		//var modal = $('<div id="appt-modal">' + weekend + '<iframe src="https://www.library.losrios.edu/scc/' + directory + '/appointments-select.php" width="100%" height="100%" frameborder="0" style="min-width:100%; height:100%;" id="if-dialog"></iframe></div>');
		if (document.documentElement.clientWidth > 640) {
					var modal = $('<div id="appt-modal">' + weekend + '<iframe src="' + url + '" width="100%" height="100%" frameborder="0" style="min-width:100%; height:100%;" id="if-dialog"></iframe></div>');
		modal.dialog({
				modal: true,
				width: w,
				height: h,
				title: 'Make an Appointment',
				open: function() {
					$('.ui-widget-overlay').on('click', function() {
						$('#appt-modal').dialog('close');
						});
				}
			});
		}
		else {
			location.href = url;
		}

	});



}());