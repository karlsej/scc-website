if (typeof(_Lib) !== 'object') {
	window._Lib = {};
}
if (typeof(_Lib.fn) !== 'object') {
	window._Lib.fn = {};
}
var form = 'https://losrios.libanswers.com/form?queue_id=3332';
var formPop = function() {
	window.open(form, 'asklibrarian', 'width=400, height=800,status=no,toolbar=no,menubar=no,top=100,left=300');
};
// text that will be placed below icon
var online = $('#chat-online');
var offline = $('#chat-offline');
var container = $('.libchat-block');
var askPage = false;
if (location.pathname.indexOf('services/ask-librarian') > -1) {
	askPage = true;
	container.appendTo($('#chat-on-page'));
}
// alt attributes set in LibChat widget
var onlineAlt = 'Ask Us';
var offlineAlt = 'Offline';
chatID = '12136'; // widget ID
chatHash = '3ed10430124d950ef2b216a68e1b18ba';
var setHover = function () {
	var askImg = $('.libchat_btn_img').attr('src');
		var askImgHover = askImg.replace('.png', '-hover.png');
		var block = $('.libchat-block');
		block.on('mouseover focusin', 'img', function() {
			$(this).attr('src', askImgHover);
		});
		block.on('mouseout focusout', 'img', function() {
			$(this).attr('src', askImg);
		});
};
$.ajax({
	url: 'https://v2.libanswers.com/load_chat.php?hash=' + chatHash,
	dataType: 'script',
	cache: true
	})
.done(function() {
	var waitForChat = setInterval(function() {
	console.log('working');
	var btn = $('.libchat_btn_img'); // button doesn't load until after page load--this is class given it by Springshare
	if (btn.length) {
		clearInterval(waitForChat);
		if (askPage) {
			btn.hide();
			$('#chat-offline').find('a').hide();
		}
		if (btn.attr('alt') === onlineAlt) { // if it is online, let will need to poll in case it goes offline
			// move text to inside anchor
			online.insertAfter(btn).show();
			// poll API for offline status
			var checkPresence = setInterval(function() {
				$.getJSON('https://losrios.libanswers.com/1.0/chat/widgets/status/' + chatID)
				.done(function(d) {
					console.log(d);
					if (d.online !== true) { 
						clearInterval(checkPresence); // this is just one-way
						online.hide(2, function() {
							if (askPage) {
								container.find('a').remove();
							}
							else {
								btn.attr({'style': '40% !important', 'alt': offlineAlt}); // change size of button
							}
							
							offline.show().appendTo(container); 
							// clone to remove event handler
							var newAnchor = btn.parent().clone();
							
							newAnchor.attr('href', form);
							btn.parent().remove();
							container.prepend(newAnchor);
							newAnchor.on('click', function(e) {
								e.preventDefault();
								formPop();
							});
				
							
						});
					}
				})
				.fail(function(a,b,c) {
					ga('localsiteTracker.send', 'event', 'libchat presence check', 'error', c);
				});
			}, 20000);
	
		}
		
		else if (btn.attr('alt') === offlineAlt) {
			clearInterval(waitForChat);
			offline.show().appendTo(container);
			offline.find('a').on('click', function(e) {
				e.preventDefault();
				formPop();
			});
		}
		setHover();

	}
	
}, 500);
	
	})
.fail(function(a,b,c) {
	console.log('failed');
	container.append('<a href="https://losrios.libanswers.com/form?queue_id=3332"><img class="libchat_btn_img" src="https://www.scc.losrios.edu/library/files/2014/03/ask-a-librarian-med.png" alt="Offline"></a>');
	offline.appendTo(container.show());
	ga('localsiteTracker.send', 'event', 'libchat script load', 'error', c);
	setHover();
	
});



//*/_Lib.fn.replaceChat = function(status) {
//	if (status.online !== true) {
//		online.hide(1, function() {
//			offline.fadeIn();
//		});
	//} ///*else {
		//$('#chat-online').hide(2, function() {
	//		$('#chat-offline').fadeIn();
		//});
////	}*/
//};
//*/


//_Lib.fn.check_presence();
//setInterval(_Lib.fn.check_presence, 20000);


