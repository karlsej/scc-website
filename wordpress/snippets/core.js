// when concatenating js files, always use this one
if (typeof(_Lib) !== 'object') {
	window._Lib = {};
}
if (typeof(_Lib.fn) !== 'object') {
	window._Lib.fn = {};
}



if (typeof(ga) !== 'function') {
	// so we don't get errors when previewing pages inside WP
	window.ga = function(a, b, c, d, e) {
		console.log(a + ', ' + b + ', ' + c + ', ' + d + ', ' + e);
	};

}
$.extend(_Lib.fn, {
			smallScreen: (function() {
				if (document.documentElement.clientWidth < 641) {
					return true;
				} else {
					return false;
				}
			}())




		},

		{
			getCookie: function(cname) {
				var name = cname + "=";
				var ca = document.cookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') c = c.substring(1);
					if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
				}
				return "";
			}
		},

		{

			setCookie: function(cname, cvalue, exdays, dom) {
				var expires; // set exdays to false for session-only cookie
				if (exdays !== false) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
					expires = ' expires=' + d.toUTCString() + ';';
				} else {
					expires = '';
				}
				var domain = '';
				if (typeof(dom) !== 'undefined') {
					domain = '; domain=' + dom;
				}
				var cStr = cname + '=' + cvalue + ';' + expires + ' path=/' + domain;
				console.log(cStr);
				document.cookie = cStr;
			}

		},
		{
			checkLocalSt: (function() { // reference: https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
				var storage = window.localStorage;
				try {
					var x = '__storage_test__';
					storage.setItem(x, x);
					storage.removeItem(x);
					return true;
				} catch (e) {
					return e instanceof DOMException && (
							// everything except Firefox
							e.code === 22 ||
							// Firefox
							e.code === 1014 ||
							// test name field too, because code might not be present
							// everything except Firefox
							e.name === 'QuotaExceededError' ||
							// Firefox
							e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
						// acknowledge QuotaExceededError only if there's something already stored
						storage.length !== 0;
				}
			}())
		},

		{
			getAllUrlParams: function(url) { // reference: https://www.sitepoint.com/get-url-parameters-with-javascript/

				// get query string from url (optional) or window
				var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

				// we'll store the parameters here
				var obj = {};

				// if query string exists
				if (queryString) {

					// stuff after # is not part of query string, so get rid of it
					queryString = queryString.split('#')[0];

					// split our query string into its component parts
					var arr = queryString.split('&');

					for (var i = 0; i < arr.length; i++) {
						// separate the keys and the values
						var a = arr[i].split('=');

						// in case params look like: list[]=thing1&list[]=thing2
						var paramNum = undefined;
						var paramName = a[0].replace(/\[\d*\]/, function(v)
						{
							paramNum = v.slice(1, -1);
							return '';
						});

						// set parameter value (use 'true' if empty)
						var paramValue = typeof(a[1]) === 'undefined' ? 'true' : a[1];

						// (optional) keep case consistent
						//    paramName = paramName.toLowerCase();
						//    paramValue = paramValue.toLowerCase();

						// if parameter name already exists
						if (obj[paramName]) {
							// convert value to array (if still string)
							if (typeof obj[paramName] === 'string') {
								obj[paramName] = [obj[paramName]];
							}
							// if no array index number specified...
							if (typeof paramNum === 'undefined') {
								// put the value on the end of the array
								obj[paramName].push(paramValue);
							}
							// if array index number specified...
							else {
								// put the value at that index number
								obj[paramName][paramNum] = paramValue;
							}
						}
						// if param name doesn't exist yet, set it
						else {
							obj[paramName] = paramValue;
						}
					}
				}

				return obj;
			}


		},


		{
			getRandomIntInclusive: function(min, max) { // reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
				min = Math.ceil(min);
				max = Math.floor(max);
				return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
			}
		});

// set home library cookie, for various uses
_Lib.fn.setCookie('homeLibrary', 'scc', 20, 'losrios.edu');

_Lib.basePrimoUrl = 'https://caccl-lrccd.primo.exlibrisgroup.com/discovery/search?vid=01CACCL_LRCCD:scc';
