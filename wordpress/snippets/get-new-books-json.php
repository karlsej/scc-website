<?php

// ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

ob_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];

// if (strpos($http_origin, 'losrios.edu') > -1)
// {  
    header('Access-Control-Allow-Origin: ' . $http_origin);
    header('Access-Control-Allow-Methods: GET, PUT, POST');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
// }
function writeItems($auth, $ti, $subj, $l, $ca, $ib, $img, $type) {
    $comma = '';
    if ($l !== '') {
        $comma = ', ';
    }
    $callnoSpan = $comma . '<span class="callno">' . $ca . '</span>';
	
	if (strpos($l, 'Ebook') === 0) {
		$l = 'Ebook';
		$callnoSpan = '';
	}
    if (strpos($l, 'Streaming') === 0) {
        $callnoSpan = '';
    }
$output = '';
	$output .= '<li class="new-book"  data-imagelink="' . $img . '" data-imagechecked="false" data-isbn="' . $ib .'">';
    $output .= $ti;
    if (!empty($auth)) {
        $output .= '<div class="author">' . $auth . '</div>';
        }
    $output .= '<div class="loc-callno"> ' . $l . $callnoSpan . '</div>';

	
/*	if ($type !== 'complete') {
		if ($subj[0] !== '') {
            echo '<ul class="subj-list">';
            for ($n = 0; $n < count($subj); $n++) {
                $subject = $subj[$n];
                echo '<li>' .$subject .' </li>';
                }
            echo "</ul>\n";
            }
	} */
        $output .= "</li>\n";

		return $output;
    }
$query = '';
if (isset($_GET['query'])) {
$query = $_GET['query'];
}
$callNoPrefix = '';
if (isset($_GET['callno'])){
$callNoPrefix = $_GET['callno'];
}
$regex = '';
if (isset($_GET['match'])) {
$regex = $_GET['match'];
}
$sort = '';
if (isset($_GET['sort'])) {
$sort = $_GET['sort'];
}

$regex = str_replace('\\\\', '\\', $regex); // weird - need to escape backslashes in str_replace
// echo $regex;
$list = '';
if (isset($_GET['list'])) {
$list = $_GET['list'];
}

// $yearPub = $_GET['year'];
$scLoc = '';
if (isset($_GET['location'])) {
$scLoc = $_GET['location'];
}

$complete = false;
if (isset($_GET['complete'])) {
$complete = true;
}
$media = '';
$listLabel = '';
if (isset($_GET['media'])) {
	$media = true;
    $listLabel = 'DVDs &amp; Streaming Video';
    }

$ebooks = false;
$listname = '';
if (isset($_GET['listname'])) {
	$listname = $_GET['listname'];
	
}
$home = '';
if (isset($_GET['home'])) {
	$home = $_GET['home'];
	
}


// echo '<br> page name is ' . $pageName . '<br>';
// echo '<br> position is ' .strpos($pageName, 'ebook');
if (strpos($listname,'ebook') !== false) {
    $ebooks = true;
    }

// echo '<br>ebooks is ' .$ebooks;
$jsonRoot = 'https://wserver.scc.losrios.edu/~library/new-acquisitions/lists/';
// $bookspage = 'http://www.scc.losrios.edu' . $page;
// $bookspage = 'index.php';
$file = $jsonRoot . $listname . '.json';
// $file = '2015-may-july.json'; // for testing
// echo '<br>' .$file;
// get file date so that locations can be hidden after a month or two. for some reason I need to create the date, turn it into a strong and then back into a time? otherwise I get error that object couldn't be converted to int

$h = get_headers($file, 1);
// $h = implode($h);
// echo $h;
$fileDate = NULL;
if (!(!$h || strstr($h[0], '200') === false)) {
    $fileDate = new \DateTime($h['Last-Modified']);//php 5.3
    }
$dateString = $fileDate->format('Y-m-d');
// echo $fileDate . "<br>";
if (strtotime($dateString) < strtotime('2 months ago')) {
    // echo '<br>old file';
    $fileAge = 'old';
    }
    else {
        $fileAge = 'young';
        // echo '<br>not old';
        }
        
// copy file content into a string var
$json_file = file_get_contents($file);
// convert the string to a json object
$data = json_decode($json_file);
// copy the posts array to a php var
$books = $data->books;

// sortinng functions http://stackoverflow.com/a/4282423
function cmpauth($a, $b) // 
{
    return strcmp($a->AUTH, $b->AUTH);
}
function cmptitle($a, $b) 
{
    return strcmp($a->TITLE, $b->TITLE);
}
function cmpcallno($a, $b)  
{
    return strcmp($a->CALL, $b->CALL);
}
if (($sort === 'alpha') || ($home !== '')) {
    usort($books, 'cmptitle');
}

else {
    usort($books, 'cmpcallno');
}
$output = '';
$output .= '<div id="new-books-section">';
$output .= '<div id="pager"></div>';
$output .= '<ul id="scc-new-books-list">';
foreach($books as $book) { // store JSON values in variables
    $callno = $book -> CALL;
    $location = $book -> LOC;
    $author = $book -> AUTH;
//	$book -> ED = array();
    $editor = implode('; ',$book -> ED);
    if ($author === '') {
        $author = $editor;
    }
    $author = str_replace(', author.', '', $author); // should probably take care of this when making JSON file
    $author = str_replace(' author.', '', $author);
    $title = $book -> TITLE;
    $subjects = $book -> SUBJ;
    $year = $book -> YEAR;
    $recordno = $book -> REC;
    $isbn = $book -> ISBN;
	 $url = '';
	 if (isset($book -> URL)) {
    $url = $book -> URL;
	 }
    if (strlen($isbn) === 9) {
        $isbn = '0' . $isbn;
    }
    $dvd = '';
    $edsDB = 'cat01047a';
    $dbName = 'OneSearch'; // for title attributes
    $accessNo = 'lrois.' . $recordno; // for our catalog items
    $imgURL = '//covers.openlibrary.org/b/isbn/' . $isbn . '-M.jpg';
    switch ($location) {
        case 'scbk':
        $loc = '3rd Floor Circulating';
        break;
        case 'scnew':
        $loc = '2nd Floor New Books';
        break;
        case 'sv2h':
        $loc = 'Reserve Collection';
        break;
        case 'sv2hl':
        $loc = 'Reserve Collection';
        break;
		case 'sv1d':
        $loc = 'Reserve Collection';
        break;
		case 'sv3d':
		$loc = 'Reserve Collection';
		break;
        case 'sv1w':
        $loc = 'Reserve Collection';
        break;
        case 'srref':
        $loc = '2nd Floor Reference Shelves';
	break;
	case 'scbro':
	$loc = '2nd Floor Paperbacks';
	break;
	case 'smdvd':
	$loc = 'Circulating DVDs';
	$dvd = '<img src="//wserver.scc.losrios.edu/library/wp-res/dvd.png" alt="dvd" class="dvd-icon" height="16" width="16">'; // icon that displays after dvd items
	break;
	case 'smres':
	$loc = 'Reserve Collection';
	$dvd = '<img src="//wserver.scc.losrios.edu/library/wp-res/dvd.png" alt="dvd" class="dvd-icon" height="16" width="16">';
	break;
case 'wbvid':
    $loc = 'Streaming Video';
    $dvd = '<img src="//www.library.losrios.edu/scc/wordpress/img/video-icon.png" alt="streaming video" class="dvd-icon">';
    $callno = '';
    break;
        case 'scjez':
        $loc = '2nd Floor Easy Reading';
	break;
	case 'scjuv':
	$loc = '2nd Floor Juvenile';
	break;
	case 'ebk':
	$loc = $year;
	$edsDB = 'nlebk';

	$imgURL = '//rps2images.ebscohost.com/rpsweb/othumb?id=NL$'. $recordno . '$PDF&s=d'; // reliable image source for ebsco ebooks
	$accessNo = $recordno;
	break;
		case 'wbebk':
		$loc = 'Ebook';
		break;
	case 'scdis':
	$loc = '3rd Floor Circulating'; // because they're not display books for very long
	break;
	default:
	$loc = '';
        }
	if ($fileAge === 'old') {
		$loc = '';
	}
	if ($media !== '') {
		$dvd = '';
	}
       
        $heblink = 'http://0-hdl.handle.net.lasiii.losrios.edu/2027/heb.' . $recordno;
	$epRoot = 'search.ebscohost.com';
	if ($location === 'ebk') {
		$epRoot = 'ezproxy.losrios.edu/login?url=https://' . $epRoot;
	}
        $eplink = 'https://' . $epRoot . '/login.aspx?authtype=ip,guest&custid=sacram&groupid=main&profile=eds&direct=true&db=' . $edsDB .'&AN=' . $accessNo . '&site=eds-live&scope=site';
	if ($location === 'heb') {
            $dbName = 'Humanities eBook';
            $eplink = $heblink;
	$loc = $year;
            }
            $goToDb = 'Go to item record in ' . $dbName ;
            if (!empty($url)) {
              $eplink = $url;
              $goToDb = 'Go to item';
            }
	$titleLine = '<a title="' . $goToDb . '" class="new-book-link" href="' . $eplink .'">' . $title .'</a>' .  $dvd;

	if ($location === 'ebk') {
		$epub = str_replace('PDF', 'EPUB', $imgURL);
		$loc = $year;
	$imgsrc = $imgURL;
        $imgLink =  $imgsrc;
        // alternative link sources - dont' seem to do any better. should try Google books but requires more complicated js
        // $imgsrc = '<img alt="" src="http://covers.librarything.com/devkey/d8144a672ea3b2b10da882f090364d87/medium/isbn/' . $isbn . '" class="jacket">';
        // $imgsrc = '<img alt="" src="http://contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&Password=ebsco-test&Return=T&Type=M&Value=' . $isbn . '" class="jacket">';
	}
	else {
		$imgLink = '';
	}
	if ($location === 'wbvid') {
		if (strpos($url,'kanopy') !== false) {
			$arr = explode('/', $url);
			$num = array_pop($arr);
			$imgLink = '//www.kanopy.com/node/' . $num . '/external-image';
			$loc = 'Streaming video';
		}
	}
	if ($media !== '') {
            // would be good to put this in a function somehow... because need to repeat it frequently.
            if (($location === 'smdvd') || ($location === 'smres') || ($location === 'wbvid')) {
            $output .=    writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'media');
                    }
                }
                elseif ($query !=='') { // match query to author, title, subjects
                 //   writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'query', $query);
                    $query = strtolower($query);
                    $queryTrunc = str_replace('*', '', $query);
                    $queryTrunc = preg_replace('/s$/', '', $queryTrunc);
                    $authorLC = strtolower($author);
                    $titleLC = strtolower($title);
                    $subjectString = implode(' ', $subjects);
                    $subjectString = strtolower($subjectString);
                    // match these strings only at beginning of word, to avoid so many false positives
                    if ((strpos($authorLC, $queryTrunc)> -1) || (preg_match('/(^| )' . $queryTrunc . '/', $titleLC) === 1) || (preg_match('/(^| )' . $queryTrunc . '/', $subjectString) === 1)) {
                     $output .=   writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'query');
                        }
                        }
                elseif ($callNoPrefix !== '') { // match only beginning of call numbers. Note also they need to be capitalized.
                    if (strpos($callno, $callNoPrefix) === 0) {
                     $output .=   writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'callno');
                        }
                        }
		elseif ($list !== '') { // special lists. Only relies on call numbers. would be desirbale to incorporate keyword queries
                    //$limit = '';
					
					$lists = array(
						array(
							'abbr' => 'admj',
							'regex' => '/^(^HV6[1235][0-9][0-9][0-9]|HV[789][0-9][0-9][0-9]|KF9[234678])/',
							'label' => 'Criminal Justice'
						),

						array(

							'abbr' => 'bus',
							'regex' => '/^HD[12345678][123456789][123456789]|^HD[0][123][123456789]|^HD1[123][123456789][12345]|^H[GHIJ]|^HF5[12345789]|TS/',
							'label' => 'Business, Management, Marketing'
						),

						array(

							'abbr' => 'cis',
							'regex' => '/^(QA76|TK5105)/',
							'label' => 'Computer Science'
						),

						array(

							'abbr' => 'food',
							'regex' => '/^(TX([3][4-9]|[4-9])|(QP14[1-6])|RA784|RJ206)/',
							'label' => 'Food & Cooking'
						),

						array(

							'abbr' => 'lgbt',
							'regex' => '/^(HQ7[3-7][\s|\.]|JK723\.H6|N72\.H64|PN1590\.G39|PN1650|PN1995\.9\.(H55|I43)|PS153 \.S39|RC883|GV708\.8|HV6250\.4\.H66)/',
							'label' => 'LGBTQ Studies'
						),

						array(

							'abbr' => 'mathstat',
							'regex' => '/^QA[123456]|^QA9[3456789]|^HA/',
							'label' => 'Math & Statistics'
						),

						array(

							'abbr' => 'fashion',
							'regex' => '/^(TT|GT525|N8217\.C6)/',
							'label' => 'Fashion & Clothing'
						),

						array(

							'abbr' => 'design',
							'regex' => '/^(NC(9[56789]\d|100[123]|18\d\d)|(N[EK])|TT([123]\d)|Z116\.A[345])/',
							'label' => 'Design & Making'
						),

						array(

							'abbr' => 'enviro',
							'regex' => '/^(GE|GF|HC79\.E5|TJ163|TD|QC903)/',
							'label' => 'Environment & Sustainability'
						),

						array(

							'abbr' => 'tafilm',
							'regex' => '/^(PN1[56789]\d\d|PN2\d\d\d|PN3[012]\d\d)/',
							'label' => 'Theatre, Film, Television',
							'limit' => 'smdvd'
						)

					);

							for ($int = 0; $int < count($lists); $int++) {
								if ($lists[$int]['abbr'] === $list) {
									if ($location !== $lists[$int]['limit']) {
										if (preg_match($lists[$int]['regex'], $callno) === 1) {
											$output .= writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'list');

										}
									}
									$listLabel = $lists[$int]['label'];

								}

							}
		}
                elseif ($scLoc !== '') { // used for dvds, reserves, ref
                    if (strpos($location, $scLoc)===0) {
                     $output .=   writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'location');
                        }
                        }
                elseif ($regex !== '') { // useful in testing different callnumber regexes. Leaving it in does no harm
                    if (preg_match('/' . $regex . '/', $callno) === 1) {
                    $output .=    writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'regex');
                    }
                    }
               elseif ($complete === true) { // default view - doesn't include images
                    $output .=  writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'complete');
                    }
                    
                    
                    else {
                       $homeReg = array("^PN19", "^E18", "^N[CD]", "^H[FG]", "^D");
                       $rand_reg = $homeReg[mt_rand(0, count($homeReg) - 1)];
                   //    $rand_reg = array_rand($homeReg);
                        if (preg_match("/" . $rand_reg . "/", $callno))  {
			$output .= writeItems($author, $titleLine, $subjects, $loc, $callno, $isbn, $imgLink, 'default');
                    
                        }

                        }
					
                        
                    }
						  $output .='</ul>';
						  $output .= '</div>';
						  						  $output = str_replace('"scc-new-books-list"', '"scc-new-books-list" data-callnoprefix="' . $callNoPrefix .'" data-query="' . $query .'" data-speciallist="' . $listLabel . '" data-location="' . $scLoc . '" data-ebooks="' . $ebooks .'"', $output );
						  echo $output;
 ob_end_flush();


?>