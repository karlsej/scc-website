
/* improve styling of submit buttons */
$('#tabs input[type=submit]').button();


$('#essentials a').on('mouseover focusin', function() {
	var a = $(this).attr('title');
	if (typeof(a) !== 'undefined') { // title attributes sometimes disappear when webmaster makes changes... so checking for existence of attribute keeps blank tooltip from appearing
		_Lib.fn.showToolTip($(this), a);
	}
});

// set home library cookie, for various uses
_Lib.fn.setCookie('homeLibrary', 'scc', 20);


// set primary tab
var lastActiveTabID = _Lib.fn.getCookie('_Lib-active-tab');
var lastActiveTab;
if (lastActiveTabID !== '') { // let tab selection persist for session
	var tabArr = lastActiveTabID.split('-');
	lastActiveTab = parseInt(tabArr[2], 10) - 1; // tab ids are not zero-based so need to subtract
	activeTab = lastActiveTab;
} else {
	activeTab = 3;
}
$('#tabs').tabs({
	active: activeTab
});
$('.ui-tabs-anchor').on('click', function() {
	_Lib.fn.setCookie('_Lib-active-tab', $(this).attr('id'), false);

});
$('#eds-search, #cat-search, #media-search').on('submit', function(e) { // if users do not select autocomplete item and instead click search or enter
	e.preventDefault();
	$('#loader').show();
	var kw = $(this).find('input[type="text"]').val();
	_Lib.fn.submitSearch(kw, $(this));
});
// eds autocomplete for free-entry fields in search forms
_Lib.fn.autoComp($('#la_qentry'));
_Lib.fn.autoComp($('#media-input'));
_Lib.fn.autoComp($('#cat-input'));
_Lib.fn.autoComp($('#TitleWords'));



// show today's hours on smaller screens
if ($(document).width() < 725) {
	_Lib.fn.showToday(function(h) {
		$('<p />').html(h + ' <a href="#text-5" id="scroll-hours">&#40;more info&#41;').hide().prependTo($('.mainBlock')).fadeIn();
		$('#scroll-hours').on('click', function(e) { // full hours widget displays downscreen on mobile, so link to it
			e.preventDefault();
			$('html, body').animate({
				scrollTop: $('#text-5').offset().top
			}, 400);
		});

	});

}
// essentials analytics tracking
$('#essentials a').on('click', function() {
	var a = $(this).text();
	ga('localsiteTracker.send', 'event', 'essentials', 'click', a);
});

// three-column links analytics tracking
$('.col3linklistitem a').on('click', function() {
	var label = $(this).closest('li').find('.col3linktitle').text();
	ga('localsiteTracker.send', 'event', 'columnlinks', 'click', label);


});

// socal media links analytics tracking
$('.ci-socials-ignited a').on('click', function() {
	var a = $(this);
	var b = '';
	if (a.find('i').hasClass('fa-facebook')) {
		b = 'Facebook';
	} else if (a.find('i').hasClass('fa-twitter')) {
		b = 'Twitter';
	}
	if (b !== '') {
		ga('localsiteTracker.send', 'event', 'sidebars', 'click', b);
	}
});
_Lib.fn.showNote = function(obj) {
	{
	/*
	 call using e.g.
	 _Lib.fn.showNote({
	 message: 'Due to scheduled maintenance, access to databases may be interrupted the morning of Friday, May 25.',
	 start: '2018-05-21',
	 end: '2018-05-26' // this is the beginning of the day, so if you want it to show that day, must set to the next day
	 });
	 */
	var cName = 'dbHideAlert';
	if (_Lib.fn.getCookie(cName) !== 'hide')
	{
		var d = new Date(); // time when page is viewed
		var parseDate = function(str)
		{
			var arr = str.split('-');
			return new Date(arr); // for some reason this doesn't work if you include more than year-month-date
		};
		var start = new Date(1970);
		// start and end properties are optional
		if (obj.start)
		{
			start = parseDate(obj.start);
		}
		var end = new Date(3000);
		if (obj.end)
		{
			end = parseDate(obj.end);
		}
		//console.log(start);
		//console.log(end);
		var text = obj.message || '';
		if ((d >= start) && (d <= end) && (text !== ''))
		{
			$('<p id="db-alert" role="alert" style="display:none;">' + text + ' <button type="button" id="message-dismiss">Hide this message</button></p>').insertBefore($('#tabs')).fadeIn();
			$('#message-dismiss').on('click', function()
			{
				$('#db-alert').fadeOut();
				_Lib.fn.setCookie(cName, 'hide', 2); // cookie expires in just two days
			});
		}
	}


}
};
