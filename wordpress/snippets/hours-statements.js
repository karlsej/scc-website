_Lib.fn.showToday(function(h) {
	$('<p id="todays-hours">' + h + '</p>').prependTo($('.mainBlock')).fadeIn();
});
$('#calendar-open').button().on('click', function() {
	ga('localsiteTracker.send', 'event', 'hours page', 'click', 'full calendar');
	if (!(document.getElementById('libCalScript'))) { // load script - this only happens if the button is clickecd
		var scr = document.createElement('script');
		scr.id = 'libCalScript';
		head = document.getElementsByTagName('head')[0];
		scr.src = '//api3.libcal.com/js/hours_month.js?002';
		scr.async = false; // needs to load for jQuery plugin to work
		head.appendChild(scr);
	}
	var calDialog = setInterval(function() {
		if (typeof($.LibCalHoursCal) === 'undefined') {
			//code
		} else {
			clearInterval(calDialog);
			new $.LibCalHoursCal($('#s-lc-mhw462'), {
				iid: 462,
				lid: 4504,
				months: 4,
				show_past: 1
			});
			var a = $(window).width();
			var newWidth;
			if (a > 900) {
				newWidth = a * 0.8;
			} else {
				newWidth = a;
			}
			if (newWidth > 1000) { // set max width at 1000 px - larger than that doesn't work
				newWidth = 1000;
			}
			$('#s-lc-mhw462').dialog({
				modal: true,
				width: newWidth,
				show: {
					effect: 'fadeIn'
				},
				position: {
					at: 'center top'
				},
				title: 'Full Calendar',
				open: function() {
					var wait = setInterval(function() {
						if (!($('#s-lc-mhw-cont').length)) {} else {
							clearInterval(wait);
							$('.s-lc-mhw-days').html($('.s-lc-mhw-days').html().replace(/td/g, 'th')); // fix Sprinshare's bad accessibility by creating th where needed
							$('.btn').button();
							$('.s-lc-time').each(function() {
								($(this).html($(this).html().replace(/am/g, ' am').replace(/pm/g, ' pm'))); // SCC style guide
							});
							var currentTbl = $('.s-lc-mhw-c table:visible');
							if (currentTbl.text().indexOf('Recess') > -1) {
								var recessNote = $('#recess-fine-note').clone();
								recessNote.removeAttr('id').insertAfter(currentTbl).show();
							}
						}
					}, 50);
				}
			});
		}
	}, 50);
});
(function() {
// animate and emphasize current closures
var d = new Date();
var m = d.getMonth();
var month;
switch (m) {
	case 0:
		month = 'January';
		break;
	case 1:
		month = 'February';
		break;
	case 2:
		month = 'March';
		break;
	case 3:
		month = 'April';
		break;
	case 4:
		month = 'May';
		break;
	case 5:
		month = 'June';
		break;
	case 6:
		month = 'July';
		break;
	case 7:
		month = 'August';
		break;
	case 8:
		month = 'September';
		break;
	case 9:
		month = 'October';
		break;
	case 10:
		month = 'November';
		break;
	case 11:
		month = 'December';
		break;
	default:
		month = '';
}
$('#closures li').each(function() {
	var a = $(this);
	if (a.text().indexOf(month) > -1) {
		a.addClass('current').animate({
			fontSize: '140%'
		}, 'slow');
	}
});
}());