(function()
{
	var params = _Lib.fn.getAllUrlParams(location.href);
	console.log(params);
	var format = params.type || '';
	var author = '';
	if (params.author) {
		author = decodeURIComponent(params.author);
	}
	var title = '';
	if (params.title) {
		title = decodeURIComponent(params.title);
	}
	var isbn = params.isbn || '';
	var journal = '';
	if (params.journal) {
		journal = decodeURIComponent(params.journal);
	}
	var volume = params.vol || '';
	var issue = params.issue || '';
	var year = params.date || '';
	var db = params.db || '';
	var an = params.an || '';

	$('#vfb-228').val(author);
	$('#vfb-229').val(title);
	$('#vfb-232').val(year);
	$('#vfb-230').val(journal);
	if ((an !== '') && (db !== ''))
	{
		var url = 'http://search.ebscohost.com/login.aspx?authtype=ip,guest&direct=true&site=eds-live&custid=sacram&groupid=main&db=' + db + '&AN=' + an + '&scope=site';
		$('#vfb-231').val(url);
		$('#form-fill-note').html('<strong>The form has been partially filled out with information from the article you clicked from. Please check it for accuracy and fill in any empty fields.</strong>'); // must create element with this id on the page.
	}
	if (/article|journal|publication|periodical/i.test(format))
	{
		$('#vfb-237-2').prop('checked', true);
		$('#vfb-238').val(volume);
		$('#vfb-239').val(issue);

	}
	else if (format.indexOf('ook') > -1)
	{
		$('#vfb-237-1').prop('checked', true);
		$('#vfb-241').val(isbn);

	}


}());