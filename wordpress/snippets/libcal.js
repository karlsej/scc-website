if (typeof(_Lib.fn.fixFormatting) !== 'function') {
	_Lib.fn.fixFormatting = function(str) {
		str = str.replace(/am/g, ' am').replace(/pm/g, ' pm'); // complies with SCC style guide - space between number and am/pm
		str = str.replace('C ampus', 'Campus'); // because it has an am in it
		str = str.replace(/for first/ig, 'for the first');
		str = str.replace(/\-/g, ' &ndash;'); // use proper html entities
		return str;
	};
}
if (typeof(_Lib.fn.showToday) !== 'function') {
	_Lib.fn.showToday = function(callback) { // used on home page and on hours page
		$.getJSON('https://api3.libcal.com/api_hours_today.php?iid=462&lid=4504&format=json&callback=?', function(h) {
			// console.log(h);
			var output;
			var a = h.locations[0].rendered; // using rendered view is easiest
			if ((a.indexOf('Recess') > -1) || (a.indexOf('Closed') > -1)) {
				output = 'Closed Today';
			} else {
				if (h.locations[0].times.note) {
					a.replace(/pm.*/, 'pm'); // get rid of any notes in rendered view
				}
				a = _Lib.fn.fixFormatting(a);
				output = 'Hours Today: ' + a;
			}
			callback(output);
		});
	};
}