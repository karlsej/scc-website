
$('#essentials a').on('mouseover focusin', function() {
	var a = $(this).attr('title');
	_Lib.fn.showToolTip($(this), a);
});


// reserves tab is default  
$('#tabs').tabs({
	active: 2
});
/* improve styling of submit buttons */ 
$('#tabs input[type=submit]').button();

// eds autocomplete for free-entry fields in search forms
_Lib.fn.autoComp($('#media-input'));
_Lib.fn.autoComp($('#cat-input'));
_Lib.fn.autoComp($('#TitleWords'));
$('#cat-search, #media-search').on('submit', function(e) { // if users do not select autocomplete item and instead click search or enter
	e.preventDefault();
	$('loader').show();
	var kw = $(this).find('input[type="text"]').val();
	_Lib.fn.submitSearch(kw, $(this));
});

// essentials analytics tracking
$('#essentials a').on('click', function() {
	var a = $(this).text();
	ga('localsiteTracker.send', 'event', 'essentials', 'click', a);
});
