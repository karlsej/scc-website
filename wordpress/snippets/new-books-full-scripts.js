_Lib.new_books_full = {
	fn: {
		singlePage: (function() { // hide pagination when user clicks show all
			$('#pager').smartpaginator({
				totalrecords: $('.new-book').length,
				recordsperpage: 20000,
				datacontainer: 'scc-new-books-list',
				dataelement: '.new-book'
			});
			_Lib.new_books_full.showAll.hide();
			$('.btn:contains("Next")').attr('style', 'display:none !important');
			$('.btn:contains("Prev")').attr('style', 'display:none !important');
			var singPgStr = '#singlepage';
			if (window.location.href.indexOf(singPgStr) === -1) {

				var url = window.location.href.replace(/$/, singPgStr);
				window.location.href = url;
			}
			this.getImages();
		}),

		changeTitles: (function(text) { // make it so heading and title reflect paramaters used
			var title = $('title').html();
			var newTitle = title.replace(/^/, text + ' - ');
			$('title').html(newTitle);
			var pageTitle = $('.entry-title').html();
			var newPageTitle = pageTitle.replace(/^/, text + ' &#8211; ');
			$('.entry-title').html(newPageTitle);
		}),

		hideButton: (function(a, b) {
			var firstNo = $('#pager span:nth-of-type(1)');
			var ofNo = $('#pager span:nth-of-type(3)');
			var totalNo = $('#pager span:nth-of-type(5)');
			var firstNoAsNo = parseInt(firstNo.text()); // apparently I need a radix paramater, need to look into that
			var ofNoAsNo = parseInt(ofNo.text());
			var totalNoAsNo = parseInt(totalNo.text());
			// console.log(ofNoAsNo);
			// console.log(totalNoAsNo);
			if (ofNoAsNo === totalNoAsNo) {
				a.attr('style', 'display:none !important');
			}
			if (firstNoAsNo === 1) {
				b.attr('style', 'display:none !important');
			}
		}),

		post: (function(path, parameters) { // Post to the provided URL with the specified parameters, for emailing. http://stackoverflow.com/a/5533477
			var form = $('<form />');
			form.attr(
					  {"method" : "post",
					  "action": path}
					  );
			//   form.attr("target", "_blank");
			$.each(parameters, function(key, value) {
				var field = $('<input />');
				field.attr(
						   {"type": "hidden",
						   "name": key,
						   "value": value}
						   );
				form.append(field);
			});
			// The form needs to be a part of the document in
			// order for us to be able to submit it.
			$(document.body).append(form);
			form.submit();
		}),

		makeOpenLibLink: (function(link, isbn, linkEl) {
			if ((isbn !== '') && (isbn != '0')) {
				var openLibLink = $('<img alt="" class="jacket" src="//covers.openlibrary.org/b/isbn/' + isbn + '-M.jpg?default=false" onerror="this.style.display=\'none\'; this.setAttribute(\'src\', \'//contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&Password=ebsco-test&Return=1&Type=M&Value=' + isbn + '\'); this.removeAttribute(\'style\'); this.setAttribute(\'onerror\', \'this.style.display=&quot;none&quot;\');">');
				openLibLink.prependTo(linkEl);
				_Lib.new_books_full.fn.checkCookie();
			}

		}),
		epubSub: (function(el) {
			el = $(el);
			var oldSrc = el.attr('src');
			if (oldSrc.indexOf('PDF') > -1) {
				var newSrc = oldSrc.replace('PDF', 'EPUB');
				el.attr('src', newSrc);
				el.attr('onerror', 'this.style.display=&quot;none&quot;');
			}

		}),
		getImages: (function() {
			//console.log('retrieving');
			var googleAPI = "https://www.googleapis.com/books/v1/volumes?key=AIzaSyDCwiS6wHiD9HMoJtT4nVFnh5YaLMCmZo0&q=isbn:";
			if (location.href.indexOf('complete') === -1) {
				if (!($('#new-books-section').length)) {
					$('#new-books-section').after('<p id="img-attribution">Book images from <a href="//www.openlibrary.org/">Open Library</a>, EBSCO, or <img src="//www.scc.losrios.edu/library/wp-content/blogs.dir/109/files/2015/11/powered_by_google_on_white.png" alt="Powered by Google"></p>');
				}
				setTimeout(function() { // this is necessary because when using the paging feature, the visibility needs to switch before this fires
					$('.new-book').each(function() {
						var a = $(this);

						if (a.data('imagechecked') === false) {
							if (a.is(':visible')) {
								a.data('imagechecked', true);
								var isbn = a.data('isbn');
								var linkEl = a.find('.new-book-link');
								// get link
								var link = linkEl.attr('href');

										 if (a.data('imagelink') !== '') {
											console.log('found imagelink');
											a.find('.new-book-link').prepend('<image alt="jacket" onerror="_Lib.new_books_full.fn.epubSub(this)" class="jacket" src="' + a.data('imagelink') + '">');
										}


								if ((isbn !== '') && (!(a.find('.dvd-icon').length))) {
									if (!(a.find('.jacket').length)) {
										console.log('no jacket');
										if (a.find('.dvd-icon').length) {
										_Lib.new_books_full.fn.makeOpenLibLink(link, isbn, linkEl);
									}
										else {
											//	console.log('hey!');
											// Make a ajax call to get the json data as response.
											$.getJSON(googleAPI + isbn, function(response) {
												//      console.log(googleAPI + isbn);
												// In console, you can see the response objects
												//      console.log("JSON Data: " + response.items);
												if (response.items) {
													// Loop through all the items one-by-one
													for (var i = 0; i < response.items.length; i++) {
														// set the item from the response object
														var item = response.items[i];
														if (item.volumeInfo.imageLinks) {
															//code
															var thumb = item.volumeInfo.imageLinks.thumbnail;
															if (thumb) {
																thumb = thumb.replace('&edge=curl', '').replace('http:', 'https:'); // secure Google API returns explicitly http image sources!
																var image = $('<img alt="jacket" class="jacket gImage" src="' + thumb + '">');
																image.prependTo(linkEl);
																_Lib.new_books_full.fn.checkCookie();
															}
														}

													}
												} else {

													_Lib.new_books_full.fn.makeOpenLibLink(link, isbn, linkEl);
												}
											}).fail(function() { // if we hit the api limit, it goes to openLibrary
												_Lib.new_books_full.fn.makeOpenLibLink(link, isbn, linkEl);
											});

										}
									}
									

								}

							}


						}

					});




				}, 200);
				setTimeout(_Lib.new_books_full.fn.removeOnePix, 3000);

			}

		}),
		checkCookie: (function() {
			var newWins = _Lib.fn.getCookie('newWindowLinks');
			if (newWins === 'yes') {
				var ch = document.getElementById('target-setting');
				if (ch) {
					ch.checked = true;

				}
				var links = document.querySelectorAll('.new-book-link');
				var extendForm = document.getElementById('extend-form');
				if (links) {
					for (var a = 0; a < links.length; a++) {
						links[a].setAttribute('target', '_blank');
					}
				}
				if (extendForm) {
					extendForm.setAttribute('target', '_blank');
				}
			}
			if (_Lib.fn.getCookie('onesearchDomain') === 'proxy') {
				$('.new-book a').each(function() {
					var a = $(this).attr('href');
					if (a.indexOf('.losrios.edu') === -1) {
						var b = a.replace(/^/, 'https://ezproxy.losrios.edu/login?url=');
						$(this).attr('href', b);
					}
				});
			}
		}),

		removeOnePix: (function() {

			$('.jacket').each(function() {
				if ($(this).width() === 1) {
					$(this).remove();
				}

			});
		})

	}




};



var url = location.href.split('?'); // get parameters to pass on to php script
var q = url[1];
console.log(q);
/*
params = '';
if (typeof(q) !== 'undefined') {
	params = q.split('=');
	var paramsObj = {};
	paramsObj[params[0]] = params[1];
	console.log(paramsObj);
	if (paramsObj.callno) {
		$('#callno-prefix').text(paramsObj.callno);

	}
} else {
	q = '';
}
*/
if (typeof(q) === 'undefined') {
	q = '';
}

var pathParts = window.location.pathname.split('/');
var path = pathParts.pop();
if (path === '') {
	path = pathParts.pop();
}
  //var server = 'bitbucket/scc-website/wordpress/snippets/';
 var server = 'wordpress/scripts/';
var queryStr = '';
if (q !== '') {
	queryStr = '&' + q;
}
else {
	queryStr = '&home=true';
}
console.log(path + queryStr);
var file = '//www.library.losrios.edu/scc/' + server + 'get-new-books-json.php?listname=' + path + queryStr;
console.log(file);
$.get(file)
	.done(function(data) {

		$('#loader').hide();
		$('#callno-form').after(data);
		$('#newwins').before('<a id="full-list" class="hidden"  href="' + url[0] + '?complete">View complete list</a>');
		$('#new-books-section').prepend('<button id="send-list" type="button"><img src="//www.scc.losrios.edu/library/wp-content/plugins/socials-ignited/images/square/default/16/email.png" alt=""> Email this list</button> <button id="show-all" class="hidden button" >View all results one page</button>');
		_Lib.new_books_full.recordsNo = $('.new-book').length;
		// "back to full list" div is hidden by default. unhide it when it's not the main page
		_Lib.new_books_full.container = $('#new-books-section');
		_Lib.new_books_full.bookList = $('#scc-new-books-list');
		_Lib.new_books_full.showAll = $('#show-all');


		if (window.location.href.indexOf('?') > -1) {
			if (window.location.href.indexOf('#singlepage') > -1) {
				_Lib.new_books_full.fn.singlePage();
			} else {
				$('#pager').smartpaginator({
					totalrecords: _Lib.new_books_full.recordsNo,
					recordsperpage: 10,
					datacontainer: 'scc-new-books-list',
					dataelement: '.new-book',
					initval: 0,
					length: 5,
					next: 'Next',
					prev: 'Prev',
					first: 'First',
					last: 'Last',
					onchange: _Lib.new_books_full.fn.getImages
				});		
				$('#full-list').removeAttr('class');
			}
		} else {
			$('#show-all, #send-list').hide();
			_Lib.new_books_full.container.before('<p class="explanatory">Below are a few sample items. To view more, use the search or browse controls above, or <a href="?complete">view the entire list</a>.</p>');
		}
		if (window.location.href.indexOf('smdvd') > -1) {
			$('.dvd-icon').hide();
		}

		// fill in urls in widget so widget can be sued on different list pages

		var listURLs = $('#booklist-special a').attr('href');
		if (typeof(listURLs) !== 'undefined') {
		listURLs = listURLs.replace(/^/, window.location.pathname);
		}
		// go to page on changing dropdown
		var callNoPref = $('#scc-new-books-list').data('callnoprefix');
		console.log(callNoPref);
		$('#callno-browse').val(callNoPref);
		$('#callno-browse').on('change', function() {
			location.hash = '';
			$('#callno-form').submit();
		});
		// hide previous and next links when they shouldn't show.
		var items = $('.new-book');
		$('#pager div.btn').attr('role', 'button');
		var nextButton = $('.btn:contains("Next")');
		var prevButton = $('.btn:contains("Prev")');
		//  var pageOne = $('#pager span:first-of-type');
		prevButton.attr('style', 'display:none !important');
		// message when no results
		if (items.length === 0) {
			var zeroText = '<p id="no-results">Oh no! You got no results. Try a different search or <a href="' + _Lib.new_books_full.booksPage + '">view the entire list</a>.</p>';
			$('#newwins').after(zeroText);
			nextButton.attr('style', 'display:none !important');
		} else if (items.length < 11) {
			nextButton.attr('style', 'display:none !important');
		} else {
			// show "show all" button when pagination is active

			_Lib.new_books_full.showAll.removeClass('hidden');
			_Lib.new_books_full.showAll.on('click', function() {
				_Lib.new_books_full.fn.singlePage();
			});
		}

		// jump to top when clicking "next"
		$(nextButton.add(prevButton)).on('click', function() {
			$('html, body').animate({
				scrollTop: ($('#show-all').offset().top)
			}, 300);
			_Lib.new_books_full.fn.hideButton(nextButton, prevButton);
			_Lib.new_books_full.fn.removeOnePix();
		});

		_Lib.new_books_full.fn.hideButton(nextButton, prevButton);



		var queryText = _Lib.new_books_full.bookList.data('query');
		var listText = _Lib.new_books_full.bookList.data('speciallist');
		if (callNoPref !== '') {
			if (callNoPref.length === 1) {
				var options = $('#callno-browse option');
				options.each(function() {
					if ($(this).val() === callNoPref) {
						_Lib.new_books_full.fn.changeTitles($(this).text());
					}
				});
			} else {
				_Lib.new_books_full.fn.changeTitles('Call Numbers Starting with ' + callNoPref);
			}
		} else if (queryText !== '') {
			_Lib.new_books_full.fn.changeTitles('Search Results &#8211; ' + queryText);
			$('#kw-search').val(queryText);
		} else if (listText !== '') {
			_Lib.new_books_full.fn.changeTitles(listText);
		} else if (_Lib.new_books_full.bookList.data('location') !== '') {
			if (_Lib.new_books_full.bookList.data('location').indexOf('sv') > -1) {
				_Lib.new_books_full.fn.changeTitles('Reserves');

			}

		}

		$('#target-setting').on('click', function() {
			var a = $(this);
			var b = $('.new-book-link, #extend-form');
			if (a.is(':checked')) {
				_Lib.fn.setCookie('newWindowLinks', 'yes', 30);
				b.attr('target', '_blank');
			} else {
				_Lib.fn.setCookie('newWindowLinks', 'no', 1);
				b.removeAttr('target');
			}
		});

		$('#send-list').on('click', function() {
			var clone = _Lib.new_books_full.bookList.clone();
			console.log(clone);
			clone.find('img').remove();
			clone.find('.new-book').removeAttr('data-isbn');
			clone.find('a').each(function() {
				$(this).attr('href', $(this).attr('href').replace('https://ezproxy.losrios.edu/login?url=', ''));

			});
			var type;
			if (clone.data('callnoprefix') !== '') {
				type = clone.data('callnoprefix');
			} else if (clone.data('query') !== '') {
				type = clone.data('query');
			} else if (clone.data('speciallist') !== '') {
				type = clone.data('speciallist');
			}
			if (window.location.href.indexOf('ebooks') > -1) {
				clone.find('.callno').remove();
			}
			_Lib.new_books_full.fn.post('https://wserver.scc.losrios.edu/~library/new-acquisitions/email/form.php', {
				booklist: clone.html(),
				label: type,
				url: window.location.href,
				title: $('.entry-title').text()
			});
		});

		//$("img").error(function() {
		//	$(this).hide();
		//	// or $(this).css({visibility:"hidden"}); 
		//});


			_Lib.new_books_full.fn.getImages();

			
		
		_Lib.new_books_full.fn.checkCookie(); // Google Analytics
		$('.new-book-link').on('click', function() {
			ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'item');
		});
		$('.subj-list a').on('click', function() {
			ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'subject');
		});
		$('#send-list').on('click', function() {
			ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'email list');
		});
		$('#show-all').on('click', function() {
			ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'single-page view');
		});
		if (_Lib.new_books_full.bookList.data('ebooks') === 1) {
			$('.callno, .books-only').hide();
		}
		

	})
	.fail(function(a, b, c) {
		$('#loader').hide();
        $('#callno-form').after('<p>The list could not be retrieved. Sorry for the incovenience! This problem has been logged and we will follow up on it.</p>');
        ga('localsiteTracker.send', 'event', 'new books lists', 'error', c);

	});