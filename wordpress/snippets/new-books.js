_Lib.newBooks = {
	firstRun: true,
	rotate: null,
	butImg: $('#bl-refresh img'),
	listArea: $('#bl-container'),
	// source is comma-delimited list. Export record numbers from main catalog and LRD catalog, replace b with c for main and with d for lrd. Export 856|u for EBSCO ebooks and prepend number with e
	source: '/library/documents/new-books-csv.txt',
	// source : 'new-books-csv.txt',
	// source : 'c13391185,c13392864,c13371228,c13391860,c13399500,c13396183,e1164331,e1171827,e1219223,e1259885,d13501410,d13501422,d13501434,d13501446,d13501458,d1350146x,d13501471,d13501483,d13501495,d13501501,d13501513,d13501525,d13501537', // for quick testing
	records: null,
	maxShown: 3,
	secondsTillRotate: 20,
	order: 'random',
	gaCategory: 'new books widget',
	adjustQueue: function(arr, num) { // take items shown and put them at the end of the array
		var count = 0;
		while (count < num) {
			var first = arr.shift();
			arr.push(first);
			count++;
		}
		return arr;
	},
	fallBackBooks: [{
			'title': 'A History of Sacramento City College in 100 Objects by William Doonan',
			'catalog': 'lois',
			'jacketType': 'wp',
			'jacketID': '2016/12/343-1440440-11.jpg',
			'an': '991001663389705325'
		},
		{
			'title': 'Redesigning America&#39;s community colleges: a clearer path to student success',
			'catalog': 'lois',
			'jacketType': 'ebsco',
			'jacketID': '9780674368286',
			'an': '991001633679705325'
		},
		{
			'title': 'Celebrating 100 years: Sacramento City College 1916-2016',
			'catalog': 'lois',
			'jacketType': 'wp',
			'jacketID': '2018/02/Happy-100th-d6567d92.jpg',
			'an': '991001701989705325'
		}

	],
	fallBackItems: function (el, but) {

		var insertFallBacks = function () {
			// build list string
			var output = '<ul id="booklist" style="display:none;">';
			var linkStr;
			var imgStr;
			for (var i = 0; i < _Lib.newBooks.fallBackBooks.length; i++) {
				output += '<li>';
				var a = _Lib.newBooks.fallBackBooks[i];
				if (a.catalog === 'lois') {
					linkStr = _Lib.basePrimoUrl.replace('search?', 'fulldisplay?') + '&amp;docid=alma' + a.an;
				}
				if (a.jacketType === 'ebsco') {
					imgStr = '//contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&amp;Password=ebsco-test&amp;Return=T&amp;Type=M&amp;Value=' + a.jacketID;
				}
				else if (a.jacketType === 'wp') {
					imgStr = '/library/files/' + a.jacketID;
				}
				output += '<a class="nb-fb" title="Go to record for ' + a.title +  '" href="' + linkStr + '"><img alt="book jacket" src="' + imgStr + '"></a></li>';
			}
			output += '<ul>';
			el.prepend(output);
			but.fadeOut();
			setTimeout(function () {
				$('#booklist').fadeIn();
			}, 500);


		};
		if ($('#booklist').length) {
			$('#booklist').fadeOut('fast', function() {
				$('#booklist').remove();
				insertFallBacks();
			});
		} else {
			insertFallBacks();
		}
	},
	shuffle: function(array) { // shuffle an array randomly. used for new books widget
		var currentIndex = array.length,
			temporaryValue, randomIndex;
		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;
			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}
		this.firstRun = false;
		return array;
	},
	rotateDisplay: function() {
		if (!($('.nb-fb').length))
		{
			clearInterval(_Lib.newBooks.rotate);
			clearTimeout(_Lib.newBooks.checkImgLoad);
			_Lib.newBooks.rotate = null;
			_Lib.newBooks.showJackets();
		}
		


	},
	truncTitle: function(str) {
		var arr = str.split(' ');
		if (arr.length > 8) {
			var newArr = arr.slice(0, 8);
			return newArr.join(' ') + '...';
		} else {
			return str;
		}
	},
	showJackets: function() {

		var apiQuery;
		_Lib.newBooks.butImg.attr('src', '//www.library.losrios.edu/resources/databases/loader.gif');
		apiQuery = _Lib.newBooks.makeQuery(_Lib.newBooks.records, _Lib.newBooks.maxShown, _Lib.newBooks.order);
		//    console.log(apiQuery);
		_Lib.newBooks.doQuery(apiQuery, _Lib.newBooks.listArea, _Lib.newBooks.maxShown);
		if (!(_Lib.newBooks.rotate)) {
			_Lib.newBooks.rotate = setInterval(function() {
				_Lib.newBooks.rotateDisplay();
			}, (_Lib.newBooks.secondsTillRotate * 1000));
		}
		_Lib.newBooks.listArea.on('mouseover', 'li', function() {
			clearInterval(_Lib.newBooks.rotate);
		});
	},
	makeQuery: function(array, num, ord) {
		var apiQuery;

		//  console.log(array);
		array = array.split(',');
		//  console.log('total number of records: ' + array.length);
		if ((ord === 'random') && (this.firstRun === true)) {
			array = this.shuffle(array);
			this.firstRun = false; // next time it won't be shuffled, rather shown items will be pushed to back
			this.array = array;
		}
		else {
			array = this.adjustQueue(this.array, num);			
		}
		var recordsToGet = array.slice(0, num);
		//  console.log(recordsToGet);
		for (var i = 0; i < num; i++) {
			recordsToGet[i] = recordsToGet[i].trim();
			//  console.log(recordsToGet[i]);
			//  var  = recordsToGet[i];

			// transform acc$ion numbers according to source
			recordsToGet[i] = recordsToGet[i].replace(/^c/, 'lrlc.').replace(/^d/, 'lrds.').replace(/^e/, '').replace(/^/, 'AN ');

			//   recordsToGet[i] =  recordsToGet[i].replace(/$/, ')');
			// console.log(recordsArr[i]);    
		}
		apiQuery = recordsToGet.join(' OR ');
		return apiQuery;
	},

	doQuery: function(query, listArea, maxShown) {
		//   query = encodeURIComponent(query);
		//   query = query.replace(/\(/g, '%28').replace(/\)/g, '%29');
		//  console.log(query);
		// gateway url is tied to newbooks profile in EBSCOadmin, contains only LOIS, Los Rios Digital Supplement, and Ebook Collection.
		var timeOut = setTimeout(function() {
			_Lib.newBooks.fallBackItems();
		}, 10000);
		var url = 'https://widgets.ebscohost.com/prod/encryptedkey/eds/eds.php?k=eyJjdCI6ImpLbzFmV1JjeHEwbEFNNERLQTNTNzlHS3ExejFmV1M2SEJuMUpiNGtZNFE9IiwiaXYiOiJjYmM2MmMxYjkzODJhZGI5NDAxZGQ1YWEwMGU0NDliYiIsInMiOiJiYTk3YzBjYmI3ZjRmMGM0In0=&p=c2FjcmFtLm1haW4ubmV3Ym9va3M=&s=0,1,1,0,0,0&q=search%3Fquery%3D' + encodeURIComponent(query) + '%26resultsperpage%3D' + maxShown;
		var addNoCoverClass = function(el) {
			var n = _Lib.fn.getRandomIntInclusive(1,4);
			el.addClass('no-cover no-cover-' + n.toString());
		};
		//  console.log(url);
		//  var output = '';
		$.getJSON(url)
		.done(function(data) {
			console.log(data);
			clearTimeout(timeOut);
			var items = data.SearchResult.Data.Records;
			var list = $('<ul />');
			if (items) {
				for (var i = 0; i < items.length; i++) {
					var a = items[i];
					var title = a.RecordInfo.BibRecord.BibEntity.Titles[0].TitleFull;
					var soR = a.Items[0].Data.replace(' :', ':');
					var sourceArr = a.Header.An.split('.');
					var source = sourceArr[0];
					title = title.replace(' :', ':').replace('[videorecording]', '').replace(/\. ?$/, '');
					var anAdjust = a.Header.An.replace(/(lrlc|lrds)\./, '');
					var ebscoStr = _Lib.basePrimoUrl.replace('search?', 'fulldisplay?') + '&amp;docid=alma' + anAdjust;
					//     console.log(title);
					var output = '';
					var listItem = $('<li />');
					listItem.data('source', source);
					var cLink = a.FullText.CustomLinks; // records with links here are hosted outside of EDS, so send the user directly there rather than to the EDS record
					var url = '';
					var recordLink;
					var recordFor = 'OneSearch record for ';
					if (cLink) {
						url = cLink[0].Url.replace('#?', ''); // fix for some ProQuest urls
						recordFor = '';
					}
					if (url !== '') { // los rios digital supplement (non-ebsco)
						recordLink = '<a title="Go to  record for ' + soR + '" href="' + url + '">';
					} else if (a.Header.DbId === 'nlebk') { // ebook collection
						recordLink = '<a title="Go to ' + recordFor + soR + '" href="https://ezproxy.losrios.edu/login?url=https://search.ebscohost.com/login.aspx?authtype=ip&amp;groupid=main&amp;profile=ebook&amp;direct=true&amp;db=' + a.Header.DbId + '&amp;AN=' + a.Header.An + '&amp;site=ehost-live&amp;scope=site">';
					} else { // books and vidoes on library shelves
						recordLink = '<a title="Go to ' + recordFor + soR + '" href="' + ebscoStr + '">';
					}

					output += recordLink;
					var img = a.ImageInfo;
					var altTitle = title.replace(/"/g, '&quot;');
					if (img) {
						var imgSrc = a.ImageInfo[1].Target.replace(/http(s)?:/, '').replace('Return=T', 'Return=1');
						output += '<img alt="' + altTitle + '" src="' + imgSrc + '"></a>';
					}
					// below is commented out now because links from new catalogs go through uresolver
					/*
					else if (url.indexOf('kanopy') > -1) {
						url = url.replace(/\/$/, '');
						var arr = url.split('/');
						var kanURL = '//www.kanopy.com/node/' + arr.pop() + '/external-image';
						output += '<span class="kan-title-text">' + altTitle + '</span><img class="kanopy" alt="' + altTitle + '" src="' + kanURL + '"></a>';
					}
					*/
					else {
						// title = title.replace(/ \/.*/, '');
						addNoCoverClass(listItem);
						output += _Lib.newBooks.truncTitle(title);
					}
					var itemData = a.Items;
					for (var j = 0; j < itemData.length; j++) {
						if (itemData[j].Group === 'TypPub') {
							//           console.log(itemData[j].Data);
							if (/video/i.test(itemData[j].Data) === true) {
								listItem.addClass('video');
							}
							else if (/ebook/i.test(itemData[j].Data) === true) {
								listItem.addClass('ebook');
							}
							else if ((a.Header.An.indexOf('lrds') > -1) && (/book/i.test(itemData[j].Data) === true)) {
								listItem.addClass('ebook');
							}
						}
					}
					/*       
        var pubType = a.Header.PubType;
        //     console.log(pubType);
        
        if (pubType.indexOf('Video') > -1)
        {
          listItem.addClass('video');
        }
        */
					//     console.log(output);
					listItem.html(output).appendTo(list);
				}
				var insertList = function() {
					setTimeout(function() {
						list.hide().prependTo(listArea).attr('id', 'booklist').fadeIn();
						_Lib.newBooks.addIcons();
						setTimeout(function() {
							_Lib.newBooks.butImg.attr('src', '/library/files/2016/10/refresh-icon-red.png').show();
						}, 300);
					}, 500);
					_Lib.newBooks.checkImgLoad = setTimeout(function() {
						$('#booklist img').each(function() {
							console.log('image loaded: ' + this.complete);
							if ((this.complete !== true) || (this.clientHeight < 5)) {
								addNoCoverClass($(this).closest('li'));
								var title = _Lib.newBooks.truncTitle(this.alt);
								$(this).parent().html(title);
							}
							
							});
						
					}, 4000);
				};

				if ($('#booklist').length) {
					$('#booklist').fadeOut('fast', function() {
						$('#booklist').removeAttr('id').remove();
						insertList();
					});
				} else {
					insertList();
				}


				setTimeout(function() {
					listArea.removeAttr('aria-live');
				}, 2000);
			} else {
				_Lib.newBooks.fallBackItems(listArea, _Lib.newBooks.butImg);
			}

		}).fail(function(jqXHR, textStatus, errorThrown) {
			ga('localsiteTracker.send', 'event', _Lib.newBooks.gaCategory, 'error', 'EDS API call: ' + errorThrown);
			console.log('EDS API error: ' + errorThrown);
			clearTimeout(timeOut);
			_Lib.newBooks.fallBackItems(listArea, _Lib.newBooks.butImg);
		});
	},
	addIcons: function() {
		var add = function(file, alt, aria) {
			return '<img id="' + aria + '" class="format-icon ' + alt.replace(/ /g, '-') + '" src="/library/files/'+ file + '.png" alt="' + alt + '" >';
		};
		var count = 0;
		var aria;
		$('.video a').each(function() {
			var a = $(this);
			aria = 'new-list-item-video-' + count;
			if (a.closest('li').data('source') === 'lrlc') {
				a.append(add('2018/03/white-dvd-32', 'DVD', aria));
			}
			else {
				a.append(add('2018/03/video-play-3-24', 'streaming video', aria));
			}
			a.attr('aria-describedby', aria);
			count++;
		});

		$('.ebook a').each(function() {
			aria = 'new-list-item-ebook-' + count;
			$(this).append(add('2018/04/ebook', 'ebook', aria));
			$(this).attr('aria-describedby', aria);
			count++;
	});
		
		
		
		
		
		
	}




};


// in case of errors, need to set items to show.




//	_Lib.fn.loadScript('res/jquery-ui-autocomplete'); // load $ ui autocomplete widget - but maybe should just include the code here


	var d = new Date();
	_Lib.monthDay = (d.getMonth() + 1).toString() + '-' + d.getDate().toString();
	if (_Lib.fn.checkLocalSt) {
		if ((localStorage.getItem('newBookList') !== '') && (localStorage.getItem('newBookTS') === _Lib.monthDay)) {
			_Lib.newBooks.records = localStorage.getItem('newBookList');
			_Lib.newBooks.showJackets();
		} else {
			if (/lrois|,/.test(_Lib.newBooks.source) === false) // if we put in a reference to a file rather than a comma-delimited string, need to get it first
			{
				$.ajax({
					url: _Lib.newBooks.source,
					dataType: 'text'
				}).done(function(data) {
					if (_Lib.fn.checkLocalSt) {
						localStorage.setItem('newBookList', data);
						localStorage.setItem('newBookTS', _Lib.monthDay);
					}
					_Lib.newBooks.records = data;
					_Lib.newBooks.showJackets();
					//     console.log(records);
					// convert to array
				}).
				fail(function(jqXHR, textStatus, errorThrown) {
					ga('localsiteTracker.send', 'event', 'new books widget', 'error', 'csv file ' + errorThrown + '');
					_Lib.newBooks.fallBackItems(_Lib.newBooks.listArea, _Lib.newBooks.butImg);
				});
			} else {
				//   records = source.split(',');
				_Lib.newBooks.records = _Lib.newBooks.source;
				_Lib.newBooks.showJackets();
			}



		}}
	




$('#bl-refresh').on('click', function() {
	_Lib.newBooks.listArea.attr('aria-live', 'polite');
	_Lib.newBooks.rotateDisplay();
	ga('localsiteTracker.send', 'event', _Lib.newBooks.gaCategory, 'refresh');
});
$('#bl-refresh').on('mouseover', function() {
	var img = $(this).find('img');
	var src = img.attr('src');
	src = src.replace('-red', '');
	img.attr('src', src);
	$(this).on('mouseout', function() {
		src = src.replace('.png', '-red.png');
		img.attr('src', src);
	});
});


// capture click events for items
_Lib.newBooks.listArea.on('click', 'li a', function() {
	ga('localsiteTracker.send', 'event', _Lib.newBooks.gaCategory, 'click', 'item');
});
$('.more-new-materials').on('click', function() {
	ga('localsiteTracker.send', 'event', _Lib.newBooks.gaCategory, 'click', 'more');
});