_Lib.newMaterials = {
	getAN : function(url) {
		var arr = url.split('&');
		for (var i = 0; i < arr.length; i++) {
			if (/an=/i.test(arr[i])) {
				var newArr = arr[i].split('=');
				return newArr[1];
			}
		}
	},
	replaceImages: function(img) {
		if (img.width() < 40) {
			var imgSrc = img.attr('src');
			imgSrc = imgSrc.replace('-M.jpg', '');
			var url = img.parent().attr('href');
			if (url.indexOf('db=nlebk') > -1) {
				var an = this.getAN(url);
				img.attr({
					src: 'https://rps2images.ebscohost.com/rpsweb/othumb?id=NL$' + an +'$PDF&s=d',
					onerror: '_Lib.newMaterials.epubImg(this)'
				});
				
			}
			else {
				imgSrc = imgSrc.replace('covers.openlibrary.org/b/isbn/', 'contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&Password=ebsco-test&Return=1&Type=S&Value=');
				img.attr({src : imgSrc,
						 onerror: 'this.display=\'none\''});
			}
			img.attr('onload', '_Lib.newMaterials.check1Pixel(this)');
			console.log('replaced');
		}

	},
	epubImg: function(img) {
		img.src = img.src.replace('PDF', 'EPUB');
		img.setAttribute('onerror', 'this.display=\'none\'');
	},
	check1Pixel: function(el) {
		if (el.clientWidth < 20) {
			el.style.display = 'none';
		}
	},
	getImages: function(el, limit) {
		el.find('.jacket').each(function(i) {

			if (limit === 'all') {
				_Lib.newMaterials.replaceImages($(this));
			} else {
				if (i <= limit) {
					_Lib.newMaterials.replaceImages($(this));
				}
			}
		});
	},
	getKanopyImg: function(limit) {

	var item = $('.feed-item');
	if (limit === 'all') {
		limit = 300;
	}
	item.each(function(i) {
		if (i < limit) {
			var link = $(this).find('.feed-link').attr('href');
			if (link.indexOf('kanopy') > -1) {
				if (!($(this).find('.jacket').length)) {
					link = link.replace(/\/$/, '');
					var arr = link.split('/');
					var kanImgSrc = '//www.kanopy.com/node/' + arr.pop() + '/external-image';
					$(this).find('.feed-desc').append('<a href="' + link + '"><image class="jacket" alt="item image" src="' + kanImgSrc + '" onerror="this.style.display=none;">');
				}
			}
		}
	});
			
	},
	addMatTypeClass: function(el) {
		var matTypes = [
						'ebk',
						'vid'
						];
		
		for (var i = 0; i < matTypes.length; i++) {
			if (el.find('.summary').hasClass(matTypes[i])) {
				el.addClass(matTypes[i]);
			}
		}
	}



};

$('.expand-list').on('click', function() {
	$(this).fadeOut();
	$(this).closest('.textContent').children('.newmat-list').removeClass('short-list');
	var parent = $(this).closest('div');
	_Lib.newMaterials.getImages(parent, 'all');
	_Lib.newMaterials.getKanopyImg('all');

});




$('#target-setting').on('click', function() {
	var a = $(this);
	var b = $('.feed-item a');
	if (a.is(':checked')) {
		_Lib.fn.setCookie('newWindowLinks', 'yes', 30);
		b.attr('target', '_blank');
	} else {
		_Lib.fn.setCookie('newWindowLinks', 'no', 1);
		b.removeAttr('target');
	}
});

_Lib.newMaterials.checkLinks = setInterval(function() {
	if ($('.feed-item').length) {
		clearInterval(_Lib.newMaterials.checkLinks);
		console.log('rssRow found');
		if (_Lib.fn.getCookie('newWindowLinks') === 'yes') {
			$('#target-setting').prop('checked', true);
			$('.feed-item a').attr('target', '_blank');

		}
	}
}, 200);



_Lib.fn.showRSS({
	feed: 'https://www.library.losrios.edu/scc/feeds/rss.php?list=New-Books',
	titleTag: 'p',
	containerID: 'newbooks',
	showDesc: true,
	loaded: function() {

		$('.feed-item').each(function() {
			var feedItem = $(this);
			var matTypes = ['vid', 'ebk', 'bk'];
			for (var i = 0; i < matTypes.length; i++) {
				if (feedItem.find('.summary').hasClass(matTypes[i])) {
					console.log('found: ' + matTypes[i]);
					feedItem.addClass(matTypes[i]);
				}
			}
			/*
			if (_Lib.fn.getCookie('ezproxy') !== '') {
				$(this).find('a').each(function() {
					var str = ($(this).attr('href'));
					if (/kanopy|ezproxy/.test(str) === false) {
						$(this).attr('href', str.replace(/^/, 'https://ezproxy.losrios.edu/login?url='));
					}
				});
			}
			*/

			});
		_Lib.newMaterials.waitForImg = setInterval(function() {
	if ($('.jacket').length) {
		clearInterval(_Lib.newMaterials.waitForImg);
		setTimeout(function() {
			_Lib.newMaterials.getImages($('#newbooks'), 3);
		}, 300);
	}
}, 200);
		_Lib.newMaterials.getKanopyImg(5);

	}

});




setTimeout(function() {
	$('#newbooks a').on('click', function() {
		var title = $(this).text();
		ga('localsiteTracker.send', 'event', 'new books feed', 'click', title);
	});

}, 1000);