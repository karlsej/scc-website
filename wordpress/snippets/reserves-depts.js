(function() {
		var output = '<div id="dept-dialog-container"><h2>All Departments:</h2><ul id="dept-dialog" >';
		console.log(output);
		$('#Department option').each(function() {
			var a = $(this);
			var abbr = a.val();
			var str = a.html();
			output += '<li class="dept-options"><button class="dept-dialog" data-abbr="' + abbr + '">' + str + '</button></li>';


		});
		output += '</ul></div>';

		$(output).hide().appendTo('body');
	
	$('.dept-options:first-of-type button').text('Leave Blank');
	
	
	}());

if ($(window).width() > 720) {
	$('#Department').on('click', function(e) {
	e.preventDefault();
	$('#Department option').hide();
//	var div = $(this).closest('.unit');
//	div.css('visibility', 'hidden');
	var w = $(document).width() * 0.9;
	if (w > 1000) {
		w = 1000;
	}
	var deptContainer = $('#dept-dialog-container');
	deptContainer.dialog({
		'ui-dialog': 'department-menu',
		'width': w,
		'modal': true,
		'show': {duration: 200},
		'hide': { duration: 200},
		'title': 'Select a Department',
		'open': function() {
			// recently used departments... playing around with it
				var recentDepts = _Lib.fn.getCookie('reserveDepts');
				if (recentDepts !== '') {
					$('#dept-dialog-container h2').show();
					$('#dept-dialog-container').prepend('<div id="recentdept-section"><h2 id="recentdept">Recently Searched: </h2><ul id="recent-depts"></ul></div><hr>');
					var arr = recentDepts.split(',');
					console.log(arr);
					if (arr.length > 4) {
						arr.pop();
					}
					$('.dept-dialog').each(function() {
						var code = $(this).data('abbr');
						if ($.inArray(code, arr) > -1) {

							$(this).parent().clone().appendTo($('#recent-depts'));
						}


					});
// $('#recentdept').css({'float': 'left', 'padding-right': '1em'});
					
				}
				else {
					$('#dept-dialog-container h2').hide();
				}
//				$('#reservessearch .unit:first-of-type').css('visibility', '');
			$('.ui-widget-overlay').on('click', function() {
				deptContainer.dialog('close');
			});
		},
		'close': function() {
			$('#recentdept-section, #dept-dialog-container hr').remove();
			$('#Department option').show();
			$('#CourseNumber').focus();
//			$('#reservessearch .unit:first-of-type').css('visibility', '');
		}
	});
	//    $('#dept-dialog li:first-of-type').hide();
	$('.dept-dialog').on('click', function() {
				var abbr = $(this).data('abbr').trim();
//				var text = $(this).text();
				if (abbr !== '') {
					$('#Department option[value="' + abbr + '"]').attr('selected', true);

					$('#dept-dialog-container').dialog('close');
					var setDepts = _Lib.fn.getCookie('reserveDepts');
					if (setDepts === '') {
						_Lib.fn.setCookie('reserveDepts', abbr, 1);
					}
					else {

						var arr = setDepts.split(',');
						if ($.inArray(abbr, arr) === -1) { // if the chosen department is not already in the array, remove the last element and add it, then reset the cookie
							if (arr.length > 3) {
								arr.pop();
							}
							_Lib.fn.setCookie('reserveDepts', abbr + ',' + arr, 1);
						}


					}
					if ($(this).parents('#recent-depts').length) {
						ga('localsiteTracker.send', 'event', 'home search widget', 'click', 'recent department button');
					}
				} else {
					$('#dept-dialog-container').dialog('close');
					
					$('#Department option:first-of-type').attr('selected', true);
				}

	});


});
	
	
}

