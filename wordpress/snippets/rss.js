


	_Lib.fn.extractInnerText = function(obj) { // gets text from XML nodes. was using innerHTML but does not work in IE. needed to parse RSS files
		var s = new XMLSerializer();
		var str = s.serializeToString(obj);
		var newStr = str.replace(/<(\/)?\w*>/g, '');
		return newStr;
	};


	_Lib.fn.showRSS = function(obj) { // display rss as html
		/*
		
		// defaults when initializing:
		remote: true, // sends feed to local proxy with CORS headers allowing AJAX from specified remote server
		limit: 0 // means no maximum
		offset: 0,
		showDesc: false,
		titleTag: 'p',
		containerID: '',
		feed: ''
		*/
		console.log(obj);
		var proxy = true;
		var num = 0;
		var offset = 0;
		var showDesc = false;
		var titleEl = 'p';
		var el = 'rss';
		var callback = function() {
			console.log('no callback');
		};

		if (obj.remote === false) {
			proxy = obj.remote;
		}
		if (obj.limit) {
			num = obj.limit;
		}
		if (obj.offset) {
			offset = obj.offset;
		}
		if (obj.showDesc) {
			showDesc = obj.showDesc;
		}
		if (obj.titleTag) {
			titleEl = obj.titleTag;
		}
		if (obj.containerID) {
			el = obj.containerID;
		}
		var container = $('#' + el);

		if (proxy === true) {
			obj.feed = 'https://wserver.scc.losrios.edu/library/tools/rss-proxy.php?url=' + encodeURI(obj.feed);
		}
		if (obj.loaded) {
			callback = obj.loaded;
		}
		var storeLocal = 60; // 60 minutes - when calling, can omit for 60 minutes, supply different number of minutes or use false for no caching
		if (obj.cache === false) {
			storeLocal = false; 
		}
		else if (obj.cache) {
			storeLocal = obj.cache;
		}
		var insert = function(str, stEl, fresh)
		{
			if (stEl === null)
			{


				container.append(str);
			}
			else
			{
				stEl.append(str);
				container.append(stEl);
			}


			if (fresh === true)
			{
				if (_Lib.fn.checkLocalSt && storeLocal)
				{
					localStorage[encodeURIComponent(obj.feed)] = container.html();
					localStorage[encodeURIComponent(obj.feed) + '_timestamp'] = Date.now();
				}
			}

			callback();
		};		var getRSS = function()
		{
			$.get(obj.feed)

				.done(function(data)
				{
				var xml = $(data);
				console.log(xml);

				var channel = xml.find('channel')[0];


				var feedTitle = channel.getElementsByTagName('title')[0];
				feedTitleStr = _Lib.fn.extractInnerText(feedTitle);
				if (feedTitleStr === 'Library') {
					feedTitleStr = 'The Reader';
				}
				var output = '';
				var startEl = $('<div />');
				if ((num === 0 || num > 1)) {
					startEl = $('<ul />');
				}
				startEl.attr('class', 'feed-area');
				var items = xml.find('item');
				var itemsToShow = (offset + num);
				console.log(itemsToShow);
				if (itemsToShow > items.length) {
					itemsToShow = offset + (items.length - offset);
				} else if (num === 0) {
					itemsToShow = offset + items.length;
				}
				if ((itemsToShow - offset) < 1) {
					console.log('error in rss call - no items to show');
				}

				for (var i = offset; i < itemsToShow; i++) {
					var a = items[i];
					//console.log(a);
					var itemLink = a.getElementsByTagName('link')[0];
					var itemLinkStr = _Lib.fn.extractInnerText(itemLink);
					var title = a.getElementsByTagName('title')[0];
					var titleStr = _Lib.fn.extractInnerText(title);

					var desc = a.getElementsByTagName('description')[0];
					var descStr = _Lib.fn.extractInnerText(desc);
					if ((num === 0 || num > 1)) {
						output += '<li class="feed-item">';
					}

					output += '<' + titleEl + ' class="feed-item-title">';
					var titleSnp = titleStr.replace(/ \/ .*/, '');

					output += '<a class="feed-link" title="More about ' + titleSnp + '" href="' + itemLinkStr + '">' + titleStr + '</a>';
					output += '</' + titleEl + '>';

					if (showDesc === true) {
						if (desc) {


							if (descStr.indexOf('&lt;') > -1) {
								descStr = descStr.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp.gt/g, '&gt').replace(/&amp;/g, '&');
							}
							descStr = descStr.replace(/.*CDATA\[/, '').replace(' [&#8230;]]]>', '...');
							output += '<p class="feed-desc">' + descStr + '</p>';
						}
					}
					if (num > 1) {
						output += '</li>';
					}

				}
				insert(output, startEl, true);


				})
				.fail(function(jx, status, err)
				{ // send event ot Google?
					console.log('feed error:' + err);
					ga('localsiteTracker.send', 'event', 'rss', 'error', err);

				});
		};		console.log(obj.feed);
		if ((storeLocal) && (_Lib.fn.checkLocalSt)) {
			var storageObj = localStorage[encodeURIComponent(obj.feed)];
				if ((storageObj) && (storageObj + '_timestamp')) {
					if ((Date.now() - localStorage[encodeURIComponent(obj.feed) + '_timestamp']) < storeLocal * 60 * 1000 ) { // cache in local storage for one hour or amount of time chosen when calling
						insert(storageObj, null);
					}
					else {
						getRSS();
					}
				}
				else {
					getRSS();
				}
			
		}
		else {
			getRSS();
		}

	};