

	


		var everything = '&tab=everything&search_scope=scc_everything';
		var booksVids = '&tab=books_videos_in_library&search_scope=books_videos_in_library';





	_Lib.fn.showError = function(field, text) {

		field.addClass('error').after('<div role="alert" class="errortext">' + text + '</div>').select();

	};

	_Lib.fn.reserveQValidator = function() {
		var baseUrl = _Lib.basePrimoUrl;
		var suffix = everything;
		if (location.href.indexOf('/library-catalog-search') > -1) {
			baseUrl = _Lib.basePrimoUrl + '_cat';
			suffix = booksVids;
			
		}
		$('.errortext').remove();
		$('.error').removeClass('error');
		var numExp = /^[0-9]{2,3}([.][0-9]{1})?$/; // only numbers. this accounts also for some decimals e.g. 300.1
		var alphaExp = /^\s*[A-Za-z\-_\'\/]+$/;
		var courseNum = $('#CourseNumber');
		var profName = $('#ProfessorsLastName');
		var x = courseNum.val().trim();
		var y = profName.val().trim();
		var submitForm = true; // set flag
		if ((x !== '') && (numExp.test(x) === false)) {
			_Lib.fn.showError(courseNum, 'The course number is usually 2 or 3 digits. Examples are 100, 300, and 310. If you\'re not sure, leave it blank.');
			submitForm = false;
		}
		if ((y !== '') && (alphaExp.test(y) === false)) {
			_Lib.fn.showError(profName, 'Please type <em>only the last name</em>, with no spaces or punctuation. If you\'re not sure, leave it blank.');
			submitForm = false;
		}
		if (submitForm === true) {
			var term1 = '';
			var dep = $('#Department').val();
			if (dep !== '') {
				term1 = dep + ' '; // need to add space after non-empty strings when building query
			}
			var term2 = '';
			if (x !== '') {
				term2 = x + ' ';
			}
			var term3 = '';
			if (y !== '') {
				// underscores don't count as spaces in Primo VE as they did in EDS
				y = y.replace('_', ' ');
				term3 = y + ' ';
			}
			var titleWords = $('#TitleWords').val().trim();
			var term4 = '';
			if (titleWords !== '') {
				term4 = titleWords + ' ';
			}

			var query = term1 + term2 + term3 + term4 + '"on reserve" scc';
			$('#loader').show();
			location.href =  baseUrl +'&query=any,contains,' + encodeURIComponent(query) + suffix;

		}
	};

	$('#show-kw-fields').on('click', function() {
		$(this).fadeOut('fast', function() {
			$(this).remove();
			$('#reserves-keywords label, #TitleWords').css('display', 'block').fadeIn();
			$('#TitleWords').focus();
		});
	});



	$('#reservessearch').on('submit', function(e) {
		e.preventDefault();
		_Lib.fn.reserveQValidator();

	});


	

	_Lib.fn.submitSearch = function(kw, el) {
		var baseUrl = _Lib.basePrimoUrl;
		if (location.href.indexOf('/library-catalog-search') > -1) {
			baseUrl = _Lib.basePrimoUrl + '_cat';
			
		}
		console.log(baseUrl);

		var suffix = '';
		var formID = el.attr('id');
		if (formID === 'reservessearch') {

			//          _Lib.fn.reserveQValidator(); -- could do it this way, but search would submit on selection - not sure this is desirable for reserves form
			return false;
		}
		if (kw !== '') {
			if (formID === 'eds-search') {
				if ($('#ebooks-only').is(':checked')) {
					suffix = everything + '&facet=rtype,include,books&facet=tlevel,include,online_resources';
				}
				else if ($('#scholarly-only').is(':checked')) {
					suffix = everything + '&facet=tlevel,include,peer_reviewed';
				}
				else {
					suffix = everything;
				}
				
			}
			else if (formID === 'cat-search') {
				suffix = everything + '&facet=rtype,include,books';
				if ($('#ebsco-check').prop('checked') === false) {
					suffix = booksVids + '&facet=rtype,include,books';
				}
			}
			else if (formID === 'media-search') {
				suffix = everything + '&facet=rtype,include,videos';
								if ($('#streaming').prop('checked') === false) {
					suffix = booksVids + '&facet=rtype,include,videos';
				}
			}

			//code
			// send known db queries to db page, otherwise to EDS
			var dbPatterns = /^(ebsco( )?(host)?$|proquest|academic search complete|films on demand|cinahl|j( )?stor|ethnologue|lex[ui]s(( )?nex[iu]s)?|nex[iu]s|gale($| )|gvrl|cq|onesearch|oxford art|grove|artstor|ebooks|kanopy|google scholar|business source|opposing viewpoints|socindex|psycarticles|^eric$|education research complete|greenfile|intelecom|pubmed|medline|naxos|newsbank|ovid|oxford english|oed|rcl|resources for college|science( )?direct|kanopy|digital theatre|masterfile)/i;
			//   ga('send', 'event', 'search', 'submit', kw);
			if (dbPatterns.test(kw) === true) {
				console.log('found match');
				location.href = '//www.library.losrios.edu/resources/databases/index.php?az&query=' + encodeURIComponent(kw) + '&college=scc';
			} else {
				var url =  baseUrl + '&query=any,contains,' + encodeURIComponent(kw) + suffix;
		//		console.log(kw + suffix);
				location.href = url;
			}

		} else {
			// if someone clicks submit without entering a query, go to eds start screen
			location.href =  baseUrl;
		}

		
	};


	// autocomplete for keyword search boxes

	// function to retrieve token
	_Lib.EBSCOtoken = _Lib.fn.getCookie('EBSCOauth');
	_Lib.fn.getEBSCOtoken = function() {
		$.get('https://widgets.ebscohost.com/prod/simplekey/autocomplete/token.php', {custid: 'lrautocomp'})
		.done(function (data) {
            _Lib.EBSCOtoken = data.token;
            console.log('autocomplete authorized');
            var timeout = 1800; // 1800 seconds = 30 minutes
            // Need to renew before timeout to ensure there is always a non-timed out autocomplete token
            var beforeTimeoutMultiplier = (0.9);
            var retryTime = timeout * beforeTimeoutMultiplier;
			document.cookie ='EBSCOauth=' + data.token + ';max-age=' + retryTime; // cookie expires before token does
            window.setInterval(_Lib.fn.getEBSCOtoken, retryTime * 1000); // refresh token in anticipation of repeated use. Shouldn't come up that much because users don't stay on the page that long
        }).fail(function () {
            console.log('autocomplete failed to authorize');

        });
		
	};
	$('.library-search-form input').on('focus', function() {
		if (_Lib.EBSCOtoken === '') {
			_Lib.fn.getEBSCOtoken();
		}
		});
	_Lib.fn.autoComp = function(el) {
		var waitForToken = setInterval(function() { // lazy to do it this way but didn't have the stomach for another callback...
			if (_Lib.EBSCOtoken !== ''){
				clearInterval(waitForToken);
				var form = el.closest('form');
				el.autocomplete({
					source: function(request, response) {
						$.ajax({
							url: 'https://global.ac.ebsco-content.com/autocomplete/rest/autoComplete',
							data: {
								token: _Lib.EBSCOtoken,
								term: encodeURIComponent(request.term),
								idx: 'rawqueries',
								filters: JSON.stringify([{
									name: 'custid',
									values: ['lrautocomp']
									}])
								}
								})
						.done(function(data) {
							var terms = data.terms.map(function (wrapper) {
								return wrapper.term;
								});
							response(terms);
							})
						.fail(function(a, b, c) {
							ga('localsiteTracker.send', 'event', 'eds autosuggest', 'error', c);
							});
						},
						select: function(event, ui) { // form will be processed by submit function on selection.
							if (ui.item) {
								el.val(ui.item.value);
								}
								_Lib.fn.submitSearch(el.val(), form);
								}
								});
				}
				}, 200);
		setTimeout(function() {
			if (_Lib.EBSCOtoken === '') {
				clearInterval(waitForToken);
			}
			},60 * 1000);

	};


	//	_Lib.fn.autoComp($('#la_qentry, #media-input, #cat-input'));

	// load array of prof names for autocomplete
		$.get( '//www.library.losrios.edu/resources/onesearch/reserves-search/res/autocomplete.php?college=scc')
		.done(function(data) {
			var arr = data.split(',');
			$('#ProfessorsLastName').autocomplete({
				source: function(request, response) {
					var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
					response($.grep(arr, function(item) {
						return matcher.test(item);
					}));
				},
				autoFocus: true
			});
		})
		.fail(function(a,b,c) {
			ga('localsiteTracker.send', 'event', 'reserves form autocomplete', 'error', c);
		});
