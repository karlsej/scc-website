_Lib.staff = [{
		names: /boyd|chenu|clark|goodchi|harker|hayash|howe|jue|karlse|lopez|patrice|posz|terch|wallace|watts|wergeland|woolle|yanez/i,
		butID: 'librarians',
		hash: 'librarians'
	},
	{
		names: /baler|brothert|chekmar|cross|gage|george|izzo|reyes|shutak/i,
		butID: 'lib-tech',
		hash: 'library-technicians'
	},
	{
		names: /austin|cello|gordon|howe|kinney|livas|lopez|posz|^ros|rowland|terch|xiaoli/i,
		butID: 'libr-libt',
		hash: 'libr-libt-instructors'
	}
];
_Lib.staff.chair = 'Tercho'; // change when chair changes
_Lib.fn.hashFilter = function (id) {

	for (var i = 0; i < _Lib.staff.length; i++) {
		if (id === _Lib.staff[i].butID) {
			this.filterTable($('#' + id));
		}
	}

};
_Lib.fn.filterFromHash = function () {
	for (var i = 0; i < _Lib.staff.length; i++) {

		if (location.hash.replace(/^#/, '') === _Lib.staff[i].hash) {

			this.filterTable($('#' + _Lib.staff[i].butID));
			history.pushState({
				id: _Lib.staff[i].butID
			}, null, '#' + _Lib.staff[i].hash);
		}
	}

};

_Lib.fn.filterTable = function (el) {
	var butText = el.text();
	ga('localsiteTracker.send', 'event', 'staff page', 'filter', butText);
	var folks;
	var all = $('#show-all');
	var butID = el.attr('id');
	if (butID === 'show-all') {
		$('.filtered').remove();
		$('.orig-table').fadeIn();
		all.hide();
		butTextHash = 'all';
	}
	else {
		all.show();
		for (var i = 0; i < _Lib.staff.length; i++) {
			if (butID === _Lib.staff[i].butID) {
				folks = _Lib.staff[i].names;
			}
		}
		$('.fsdirectory-table.filtered').remove();
		var filteredTable = _Lib.staff.table.clone().removeClass('orig-table').addClass('filtered');
		filteredTable.find('caption').html('Directory of ' + butText);
		var row = filteredTable.find('tbody tr');
		row.each(function () {
			if (folks.test($(this).text()) === false) {
				$(this).remove();
			}
			$('.orig-table').hide();
			filteredTable.appendTo($('#lrc-staff')).fadeIn();
		});
		//       $(this).addClass('active');
	}



};

// on initial page load, here's what happens:
var filtBut = $('#filter-buttons');
filtBut.find('button').button();
var url = '//www.scc.losrios.edu/faculty-staff-directory/?sortorder=lastfirst-name&firstrun=false&viewtype=table-view&alphasort=false&fname=&lname=&dept=&divsn=Learning+Resources&submit=Search';
if (location.hostname !== 'www.scc.losrios.edu') {
	url = 'proxy.php?url=' + encodeURIComponent('https:' + url); // for local testing
}
$.get(url).done(function (data) {
	var html = $($.parseHTML(data));
	_Lib.staff.table = html.find('.fsdirectory-table');
	_Lib.staff.table.addClass('orig-table'); // since we will be cloning it, need to identify
	_Lib.staff.table.find('tbody tr').each(function () {
		var a = $(this);
		if (/Librar|Flash|Murillo/.test(a.text()) === false) {
			a.remove(); // remove all lrc personnel excelpt those with librar* in title or are division office
		}
		var lastCell = a.find('td:last-of-type');
		lastCell.html(lastCell.html().replace('">http://', '">'));
	});
	_Lib.staff.table.find('th:first-child, td:first-child').remove(); // get rid of useless "Bio" column, won't work after ajax anyway
	_Lib.staff.table.find('th').each(function () { // remove links from table - they wouldn't work
		var a = $(this);
		a.html(a.text());
	});
	_Lib.staff.table.find('caption').html('Directory of Librarians, Library Instructors, Library Technicians and Administration'); // for accessibility

	var chairRow = _Lib.staff.table.find('tr:contains("' + _Lib.staff.chair + '")');
	chairRow.html(chairRow.html().replace('Librarian', 'Librarian / Department Chair'));
	// fix Antonio's O
	var aL = _Lib.staff.table.find('td:nth-child(2):contains("Lopez")');
	aL.html(aL.html().replace('Lopez', 'L&oacute;pez'));
	$('#loader').remove();
	$('#lrc-staff').append(_Lib.staff.table);
	filtBut.addClass('filters-displayed').show();
	if (location.hash !== '') { // if by chance someone arrives at the page with saved filtered url; unlikely but anyway...
		_Lib.fn.filterFromHash();
	}
	filtBut.find('button').on('click', function () { // show filtered directories cloned from original, or show original if show all button is clicked
		_Lib.fn.filterTable($(this));
		var butTextHash = $(this).text().toLowerCase().replace(/ &| \//g, '').replace(/ /g, '-');
		history.pushState({
			id: $(this).attr('id')
		}, null, '#' + butTextHash); // reference: https://developer.mozilla.org/en-US/docs/Web/API/History_API

	});

}).fail(function (flim, flam, flum) {
	$('#loader').remove();
	ga('localsiteTracker.send', 'event', 'staff page', 'error', flum);
});

$(window).on('popstate', function () {
	if (history.state) {
		_Lib.fn.hashFilter(history.state.id);
	}
	else if (location.hash !== '') {
		_Lib.fn.filterFromHash();
	}
	else {
		_Lib.fn.filterTable($('#show-all'));
	}
});