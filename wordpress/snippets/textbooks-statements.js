
/* improve styling of submit buttons */
$('#tabs input[type=submit]').button();

// scroll down when linking to explanation of reserves
if (location.hash === '#questions') {
	$('html, body').animate({
		scrollTop: $("#about").offset().top
	}, 800);
}

// eds autocomplete for free-entry fields in search forms

_Lib.fn.autoComp($('#TitleWords'));

_Lib.fn.showRSS({

	feed: 'https://wserver.scc.losrios.edu/library/answers/?qa=feed/tag/textbooks.rss',
	limit: 10,
	containerID: 'textbooks'

});