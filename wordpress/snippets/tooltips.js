



	_Lib.fn.showToolTip = function(el, text) {
		el.removeAttr('title');
		if (!(el.parent().find('.ttip')).length) {
			var btnID = el.attr('id');
			$('<div />').hide().attr({
				'class': 'ttip',
				'role': 'tooltip',
				'aria-labelledby': btnID
			}).html(text).appendTo(el.parent()).fadeIn('slow', function() {
				el.on('mouseout focusout', function() {
					el.parent().find('.ttip').remove();
					el.attr('title', text);
				});
			});
		}
	};






