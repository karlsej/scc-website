jQuery(document).ready(function ($)
{
    $('#exp-adv-search').on('submit', function (e)
    {
        e.preventDefault();
        var badField = $('.bad-year');
        var k = $('#expQuery');
        var p = $('#exactPh');
        var a = $('#expAny');
        if (badField.length)
        {
            badField.parent().addClass('attention');
            badField.select();
        }
        else if ((k.val() === '') && (p.val() === '') && (a.val() === ''))
        {
			var noKeys = $('#no-keywords');
			if (!(noKeys).length) {
            	$(this).prepend('<p role="alert" id="no-keywords" tabindex="0">Please enter search terms, or <a href="http://cdnc.ucr.edu/cgi-bin/cdnc?a=cl&amp;cl=CL1&amp;sp=EXP">go directly to the Express</a>.</p>');
			}
			noKeys.focus();
			noKeys.blur();
        }
        else
        {
            var exc = $('#expExc');
            var st = $('#date-start');
            var end = $('#date-end');
            var phrase = '';
            if (p.val() !== '')
            {
                phrase = '"' + p.val() + '"';
                if (k.val() !== '')
                {
                    phrase = ' ' + phrase;
                }
            }
            var exclude = '';
            if (exc.val() !== '')
            {
                exclude = ' NOT (' + exc.val() + ')';
            }
            var any = '';
            var anyVal = a.val();
            if (anyVal !== '') {
                anyVal = anyVal.replace(/ +/g, ' ');
                var anyValArr = anyVal.split(',');
                for (var i=0; i < anyValArr.length; i++) {
                    anyValArr[i] = anyValArr[i].trim();
                }
                any = anyValArr.toString();
                any = any.replace(/,/g, ' OR ').replace(/^/, '(').replace(/$/, ')');
                if ((k.val() !== '') || (p.val() !== '')) {
                    any = any.replace(/$/, ' ');
                }
            }
            var startDate = '';
            var yearPat = /^\d{4}$/;
            if (yearPat.test(st.val()) === true)
            {
                startDate = st.val();
            }
            var endDate = '';
            if (yearPat.test(end.val()) === true)
            {
                endDate = end.val();
            }
            var query = any + k.val() + phrase + exclude;
            ga('localsiteTracker.send', 'event', 'Express archive', 'search', query);
//            query = query.replace(/ /g, '+');
            var url = 'https://cdnc.ucr.edu/cgi-bin/cdnc?a=q&hs=1&r=1&results=1&txq=' + encodeURIComponent(query) + '&txf=txIN&ssnip=txt&o=20&dafdq=01&dafmq=01&dafyq=' + st.val() + '&datdq=31&datmq=12&datyq=' + end.val() + '&puq=EXP&oa=&oa=1&e=-------en--20--1--txt-txIN--------1';   
            console.log(url);
            window.open(url);
        }
    });
    $('#date-start, #date-end').on('focusout', function ()
    {
        var a = $(this);
        var pattern = /^\d{4}$/;
        if ((pattern.test(a.val()) === false) && (a.val() !== ''))
        {
            if (!(a.parent().next().is($('.year-error'))))
            {
                a.parent().after('<div class="year-error input-exp" role="alert">Enter a four-digit year such as 1938 or 1972; or leave this field blank</div>');
                a.addClass('bad-year');
            }
        }
        else
        {
            a.parent().next('.year-error').remove();
            a.removeClass('bad-year');
            a.parent().removeClass('attention');
        }
    });

    /* select a year boxes */
    var el = $('.express-browse');
    el.on('change', function ()
    {
		var year = $(this).find('option:selected').text();
		if (/\d{4}/.test(year) === true)
        {
            ga('localsiteTracker.send', 'event', 'Express archive', 'browse', year);
            window.open('http://cdnc.ucr.edu/cgi-bin/cdnc?a=cl&cl=CL2.' + year + '.01&sp=EXP&dv=year&e=-------en--20--1--txt-txIN--------1');
        }
    });
});