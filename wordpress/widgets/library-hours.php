<?php
header('Content-type: application/javascript');

 echo 'jQuery(document).ready(function($) {' . "\r\n";
?>

// for pages on which _Lib object does not yet exist
if (typeof(_Lib) !== 'object') {
	window._Lib = {};
}
if (typeof(_Lib.fn) !== 'object') {
	window._Lib.fn = {};
}

<?php

// fix formatting and showtoday

   include('../snippets/libcal' . $min . '.js');



?>

var fineMsg = '<p>If you need to pay a fine, please come to the LRC Monday &ndash; Friday between 8 am and 4:30 pm.</p>';

      _Lib.fn.showAllDays = function(thisWeek)
      { // this is only used when week is unusual, but works as a fallback
        console.log(thisWeek);
        var recess = false; // will flag this as true to include fineMsg above
        var output = '';
        output += '<ul id="libcalHours">';
        var openHours;
        for (var key in thisWeek)
        {
          if (thisWeek.hasOwnProperty(key))
          {
            if (thisWeek[key] !== thisWeek.Sunday) // Sunday is always closed
            {
              // take care of cases where there's a note
              if (thisWeek[key].times.status === 'open')
              {
                openHours = thisWeek[key].rendered;
                if (thisWeek[key].times.note)
                { 
                  if (thisWeek[key].times.hours.length === 1)
                  {
                    openHours = thisWeek[key].times.hours[0].from + ' &ndash; ' + thisWeek[key].times.hours[0].to + ' <span>' + thisWeek[key].times.note + '</span>';
                  }
                }
              }
              else if (thisWeek[key].times.status === 'closed')
              {
                if (thisWeek[key].times.note)
                {
                  openHours = 'Closed for ' + thisWeek[key].times.note;
                }
                else
                {
                  openHours = 'Closed';
                }
              }
              else if (thisWeek[key].times.status === 'text')
              {
                openHours = thisWeek[key].rendered;
                if (openHours === 'Recess')
                {
                  recess = true;
                  openHours = 'Closed for Recess*';
                }
              }
              output += '<li><strong>' + key + '</strong>: ' + openHours + '</li>';
            }
          }
        }
        output += '<li><strong>Sunday</strong>: Closed</li>';
        output += '</ul>';
        if (recess === true)
        { // include fineMsg note for recess weeks
          fineMsg = fineMsg.replace('<p>', '<p>*').replace('Monday &ndash; Friday', '');
          output += fineMsg;
        }
        output = _Lib.fn.fixFormatting(output);
        return output;
      };
      var fallBack = setTimeout(function ()
      { // if there's not response to ajax call. Springshare provides data via JSONP which does not allow for done/fail processing. http://api.jquery.com/jquery.getjson/
        var el = $('#this-weeks-hours');
        if (!($('#libcalHours').length))
        {
          el.html('<h4>Fall/Spring Semester Hours</h4><ul><li><strong>Monday &ndash; Thursday</strong>: 7:30 am &ndash; 9:30 pm</li><li><strong>Friday</strong>: 7:30 am &ndash; 5 pm</li><li><strong>Saturday</strong>: 9 am &ndash; 3 pm</li><li><strong>Sunday</strong>: Closed</li></ul>');
        }
      }, 3000);
      $.getJSON('https://api3.libcal.com/api_hours_grid.php?iid=462&format=json&weeks=2&callback=?', function (sched)
      {
        clearTimeout(fallBack); // cancel the timeout function
        var output = '';
        var thisWeek = sched.locations[0].weeks[0];
        var nextWeek = sched.locations[0].weeks[1];
        console.log(thisWeek);
        console.log(nextWeek);
        //       var openHours;
        var monThurs = '<strong>Monday &ndash; Thursday</strong>: ';
        var Sunday = '<li><strong>Sunday</strong>: Closed</li>';
        var msg;
        var openList = '<ul id="libcalHours">';
        var closeList = '</ul>';
        // most frequent thing comes first: regular semester
        if ((thisWeek.Monday.rendered === thisWeek.Tuesday.rendered) && (thisWeek.Tuesday.rendered === thisWeek.Wednesday.rendered) && (thisWeek.Wednesday.rendered === thisWeek.Thursday.rendered))
        {
          if (thisWeek.Friday.times.status === 'open')
          {
            output += openList;
            output += '<li>' + monThurs + thisWeek.Monday.rendered + '</li>';
            output += '<li><strong>Friday</strong>: ' + thisWeek.Friday.rendered + '</li>';
            output += '<li><strong>Saturday</strong>: ' + thisWeek.Saturday.rendered + '</li>';
            output += Sunday + closeList;
            //           output = _Lib.fn.fixFormatting(output);
          }
          // check for Spring recess. need to include exceptions for first day of all recesses
          else if (thisWeek.Monday.times.note)
          {
            if (thisWeek.Monday.times.note.indexOf('Spring Recess') > -1)
            {
              output += '<h3>Special Spring Recess Hours</h3>';
              output += openList;
              output += '<li>' + monThurs + ' 9 am &ndash; 3 pm</li>';
              output += Sunday + closeList;
            }
            else if (thisWeek.Monday.times.note.indexOf('Winter holiday') > -1)
            { // total closure between xmas and ny
              output += '<p><strong>Closed for Winter Holiday.</strong></p>';
            }
          }
          // this is Summer and Winter recess
          else if (thisWeek.Monday.rendered === 'Recess')
          {
            var d = new Date();
            var month = d.getMonth();
            //          month = 11; // change for testing!
            console.log(month);
            var resumeDate = '';
            var season;
            var reopenMsg;
            var resumeMsg = '';
            if ((month === 4) || (month === 5) || (month === 7)) // May - August
            {
              season = 'Summer';
              if (!(thisWeek.Saturday.times.note))
              {
                if ((month === 4) || (month === 5))
                {
                  resumeDate = 'Monday, June 10, 2019'; // fill in with start of Summer Semester
                }
                else if (month === 7)
                {
                  resumeDate = 'Saturday, August 24, 2019'; // fill in with start of Fall Semester
                }
                if (resumeDate !== '')
                {
                  resumeMsg = '<p>We will reopen on ' + resumeDate + '.</p> ';
                }
                reopenMsg = '<p><strong>Closed for Summer Recess.</strong></p>' + fineMsg + resumeMsg;
                output += reopenMsg;
              }
              else
              {
                output += openList;
                output += '<li><strong>Monday &ndash; Friday</strong>: Closed for Summer Recess. ' + fineMsg + '</li>';
                output += '<li><strong>Saturday</strong>: ' + thisWeek.Saturday.rendered;
                output += Sunday + closeList;
              }
            }
            else if ((month === 11) || (month === 0)) // December, January
            {
              season = 'Winter';
              var xMasClose = 'Tuesday, December 24, 2019 &ndash; Wednesday, January 1, 2020'; // change yearly!
              if (!(thisWeek.Saturday.times.note))
              {
                resumeDate = 'Saturday, January 18, 2020'; // fill in with start of Spring semester
                if (resumeDate !== '')
                {
                  resumeMsg = '<p>We will reopen on ' + resumeDate + '.</p> ';
                }
                reopenMsg = '<p><strong>Closed for Winter Recess.</strong></p>' + fineMsg + resumeMsg;
                output += reopenMsg;
                output += '<p><strong>Note</strong>: Campus is completely closed ' + xMasClose + '.';
              }
              else
              { // this is last week of Winter break
                output += openList;
                output += '<li><strong>Monday &ndash; Friday</strong>: Closed for Winter Recess. ' + fineMsg + '</li>';
                output += '<li><strong>Saturday</strong>: ' + thisWeek.Saturday.rendered;
                output += Sunday + closeList;
              }
            }
          }
          else if (thisWeek.Friday.times.note)
          {
            output += openList;
            msg = 'Closed for ';
            output += '<li>' + monThurs + thisWeek.Monday.rendered + '</li>';
            output += '<li><strong>Friday</strong>: ' + msg + thisWeek.Friday.times.note + '</li>';
            output += '<li><strong>Saturday</strong>: ' + thisWeek.Saturday.rendered;
            output += Sunday + closeList;
          }
          else if (thisWeek.Monday.rendered === 'Closed')
          {
            output += '<p><strong>Closed this Week.</strong></p>'; // remember, Mon.-Fri. are all the same, and it's not recess...
          }
          else if (thisWeek.Monday.times.hours[0].to === '6pm') { // summer session
            output += openList;
            output += '<li>' + monThurs + thisWeek.Monday.rendered + '</li>';
            output += '<li><strong>Friday</strong>: ' + thisWeek.Friday.rendered;
            output += '<li><strong>Saturday &ndash; Sunday</strong>: Closed</li>' + closeList;
            
          }
          else
          {
            output += _Lib.fn.showAllDays(thisWeek); // shouldn't happen, this is just in case
          }
        }
        else if (thisWeek.Thursday.times.note)
        { 
          if (thisWeek.Thursday.times.note.indexOf('Thanksgiving') > -1)
          { // note for Thanksgiving
            output += openList;
            if ((thisWeek.Monday.rendered === thisWeek.Tuesday.rendered) && thisWeek.Tuesday.rendered === thisWeek.Wednesday.rendered)
            {
              output += '<li><strong>Monday &ndash; Wednesday</strong>: 7:30 am &ndash; 9:30 pm</li>';
            }
            else
            { // in case we close early on wednesday
              output += '<li><strong>Monday</strong>: ' + thisWeek.Monday.rendered + '</li>';
              output += '<li><strong>Tuesday</strong>: ' + thisWeek.Tuesday.rendered + '</li>';
              output += '<li><strong>Wednesday</strong>: ' + thisWeek.Wednesday.rendered + '</li>';
            }
            output += '<li><strong>Thursday &ndash; Saturday</strong>: Closed for Thanksgiving Recess</li>';
            output += Sunday + closeList;
          }
          else
          {
            output += _Lib.fn.showAllDays(thisWeek); // fallback
          }
        }
        else
        {
          output += _Lib.fn.showAllDays(thisWeek); // fallback yet again
        }
        // display relevant notes about coming week
        for (var k in nextWeek)
        {
          if (nextWeek.hasOwnProperty(k))
          {
            if (nextWeek[k].times.note)
            {
              var holDate = nextWeek[k].date;
              var dArray = holDate.split('-'); // get month from date string
              var holMonth;
              var m = parseInt(dArray[1], 10); // convert to number
              var holD = dArray[2];
              var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
              holMonth = months[m - 1]; // this will get correct month from string
              if (nextWeek[k].times.status === 'closed')
              {
                msg = ' closed for ';
                if (nextWeek[k].times.note)
                {
                  output += '<p class="sched-note">Next ' + k + ', ' + holMonth + ' ' + holD.replace(/^0/,'') + ' we will be ' + msg + nextWeek[k].times.note + '.</p> ';
                }
                break;
              }
              else
              {
                output += '<p class="sched-note">Next ' + k + ', ' + holMonth + ' ' + holD.replace(/^0/,'') + ' our hours will be ' + nextWeek[k].times.hours[0].from + ' &ndash; ' + nextWeek[k].times.hours[0].to + ' for ' + nextWeek[k].times.note + '.</p>';
              }
            }
            /*           else
            {
              console.log('no closures next week');
            }
            */
          }
        }
        output = _Lib.fn.fixFormatting(output);
        $('#this-weeks-hours').html(output); // that was easy, right?
      });
<?php
echo 'console.log(_Lib);';
echo "\r\n" . '});'; // end jQuery function

?>