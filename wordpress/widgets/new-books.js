jQuery(document).ready(function ($)
{
  function readCookie(name)
  {
    name += '=';
    for (var ca = document.cookie.split(/;\s*/), i = ca.length - 1; i >= 0; i--)
    if (!ca[i].indexOf(name)) return ca[i].replace(name, '');
  }
  var proxy = readCookie('onesearchDomain');
  var catDomain = 'search.ebscohost.com';
  if (proxy === 'proxy')
  {
    catDomain = '0-' + catDomain + '.lasiii.losrios.edu';
    $('#onesearch-btn').attr('href', 'http://0-search.ebscohost.com.lasiii.losrios.edu/login.aspx?authtype=ip,guest&custid=sacram&groupid=main&profile=eds'); // OneSearch links will be proxied if session-only cookie is set
  }
  // in case of errors, need to set items to show.
  var rotate;
  var butImg = $('#bl-refresh img');
  var listArea = $('#bl-container');
  // source is comma-delimited list. Export record numbers from main catalog and LRD catalog, replace b with c for main and with d for lrd. Export 856|u for EBSCO ebooks and prepend number with e
  var source = 'http://www.scc.losrios.edu/library/documents/new-books-csv.txt';
  // source = 'new-books-csv.txt';
  // source = 'c13391185,c13392864,c13371228,c13391860,c13399500,c13396183,e1164331,e1171827,e1219223,e1259885,d13501410,d13501422,d13501434,d13501446,d13501458,d1350146x,d13501471,d13501483,d13501495,d13501501,d13501513,d13501525,d13501537'; // for quick testing
  var records;
  var maxShown = 3;
  var secondsTillRotate = 30;
  var order = 'random';
  var gaCategory = 'new books widget';

  function fallBackItems(el, but)
  {
    //  what shows if ajax calls fail. swap these out every so often
    var fallbackItem1 = '<li><a title="Go to OneSearch record for A History of Sacramento City College in 100 Objects by William Doonan" href="http://' + catDomain + '/login.aspx?authtype=ip,guest&amp;custid=sacram&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;bquery=AN+lrois.b1340001&amp;site=eds-live&amp;scope=site"><img alt="A History of Sacramento City College in 100 Objects by William Doonan" src="http://www.scc.losrios.edu/library/files/2016/12/343-1440440-11.jpg"></a></li>';
    var fallbackItem2 = '<li><a title="Go to OneSearch record for Redesigning America&#39;s community colleges : a clearer path to student success / Thomas R. Bailey, Shanna Smith Jaggars, Davis Jenkins" href="http://' + catDomain + '/login.aspx?authtype=ip,guest&amp;custid=sacram&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;bquery=AN+lrois.b1336686&amp;site=eds-live&amp;scope=site"><img alt="Redesigning America&#39;s community colleges : a clearer path to student success / Thomas R. Bailey, Shanna Smith Jaggars, Davis Jenkins" src="http://contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&amp;Password=ebsco-test&amp;Return=T&amp;Type=M&amp;Value=9780674368286"></a></li>';
    var fallbackItem3 = '<li><a title="Go to OneSearch record for The underground railroad : a novel / Colson Whitehead" href="http://' + catDomain + '/login.aspx?authtype=ip,guest&amp;custid=sacram&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;bquery=AN+lrois.b1340942&amp;site=eds-live&amp;scope=site"><img alt="The underground railroad: a novel / Colson Whitehead" src="http://contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&amp;Password=ebsco-test&amp;Return=T&amp;Type=M&amp;Value=9780385542364"></a></li>';
    var insertFallBacks = function() {
      var list = $('<ul />').attr('id', 'booklist').html(fallbackItem1 + fallbackItem2 + fallbackItem3).hide().prependTo(el);
      but.fadeOut();
      setTimeout(function ()
        {
          list.fadeIn();
        }, 500);
    };
    if ($('#booklist').length) {
      $('#booklist').fadeOut('fast', function() {
        $('#booklist').remove();
        insertFallBacks();
        });
    }
    else {
      insertFallBacks();
    }
  }
  if (/lrois|,/.test(source) === false) // if we put in a reference to a file rather than a comma-delimited string, need to get it first
  {
    $.ajax(
    {
      url: source,
      dataType: 'text'
    }).done(function (data)
    {
      records = data;
      showJackets();
      //     console.log(records);
      // convert to array
    }).
    fail(function (jqXHR, textStatus, errorThrown)
    {
      ga('localsiteTracker.send', 'event', 'new books widget', 'error', 'csv file ' + errorThrown + '');
      fallBackItems(listArea, butImg);
    });
  }
  else
  {
    //   records = source.split(',');
    records = source;
    showJackets();
  }
  //  console.log(records);

    $('#bl-refresh').on('click', function ()
    {
      listArea.attr('aria-live', 'polite');
      rotateDisplay();
    });
    $('#bl-refresh').on('mouseover', function ()
    {
      var img = $(this).find('img');
      var src = img.attr('src');
      src = src.replace('-red', '');
      img.attr('src', src);
      $(this).on('mouseout', function ()
      {
        src = src.replace('.png', '-red.png');
        img.attr('src', src);
      });
    });
  
  function showJackets()
  {
 
    var apiQuery;
    butImg.attr('src', '//www.library.losrios.edu/resources/databases/loader.gif');
    apiQuery = makeQuery(records, maxShown, order);
    //    console.log(apiQuery);
    doQuery(apiQuery, listArea, maxShown);
    if (!(rotate))
    {
      rotate = setInterval(function ()
      {
        rotateDisplay();
      }, (secondsTillRotate * 1000));
    }
    listArea.on('mouseover', 'li', function ()
    {
      clearInterval(rotate);
    });
  }

  function rotateDisplay()
  {
    clearInterval(rotate);
    rotate = null;
    showJackets();

    
  }

  function shuffle(array)
  {
    var currentIndex = array.length,
      temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex)
    {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  function makeQuery(array, num, ord)
  {
    var apiQuery;

    //  console.log(array);
    array = array.split(',');
    //  console.log('total number of records: ' + array.length);
    if (ord === 'random')
    {
      shuffle(array);
    }
    var recordsToGet = array.slice(0, num);
    //  console.log(recordsToGet);
    for (var i = 0; i < num; i++)
    {
      recordsToGet[i] = recordsToGet[i].trim();
      //  console.log(recordsToGet[i]);
      //  var  = recordsToGet[i];
      if (recordsToGet[i].indexOf('e') !== 0)
      {
        recordsToGet[i] = recordsToGet[i].slice(0, -1); // cut off check digit for non-EBSCO ebook items
      }
      // transform accession numbers according to source
      recordsToGet[i] = recordsToGet[i].replace(/^c/, 'lrois.b').replace(/^d/, 'lrd.b').replace(/^e/, '').replace(/^/, 'AN ');

      //   recordsToGet[i] =  recordsToGet[i].replace(/$/, ')');
      // console.log(recordsArr[i]);    
    }
    apiQuery = recordsToGet.join(' OR ');
    return apiQuery;
  }

  function doQuery(query, listArea, maxShown)
  {
    //   query = encodeURIComponent(query);
    //   query = query.replace(/\(/g, '%28').replace(/\)/g, '%29');
    //  console.log(query);
    // gateway url is tied to newbooks profile in EBSCOadmin, contains only LOIS, Los Rios Digital Supplement, and Ebook Collection.
    var url = 'https://widgets.ebscohost.com/prod/encryptedkey/eds/eds.php?k=eyJjdCI6InpnSVwvdEU1SEpQRnRXenZRU3RQRG93PT0iLCJpdiI6IjYwMjJiYTA3OGJkYjUxODhkYjBmMzAzN2Q2NTUyMDRjIiwicyI6IjNmNDZiMmU0NGFkZDM1ZWEifQ==&p=c2FjcmFtLm1haW4ubmV3Ym9va3M=&s=0,1,1,0,0,0&q=search%3Fquery%3D' + encodeURIComponent(query) + '%26resultsperpage%3D' + maxShown;
    //  console.log(url);
    //  var output = '';
    $.getJSON(url, function (data)
    {
      console.log(data);
      var items = data.SearchResult.Data.Records;
      var list = $('<ul />');
      if (items)
      {
        for (var i = 0; i < items.length; i++)
        {
          var a = items[i];
          var title = a.RecordInfo.BibRecord.BibEntity.Titles[0].TitleFull;
          var soR = a.Items[0].Data.replace(' :', ':');
          title = title.replace(' :', ':').replace('[videorecording]', '').replace(/\. ?$/, '');
          //     console.log(title);
          var output = '';
          var listItem = $('<li />');
          var cLink = a.FullText.CustomLinks;
          var url = '';
          var recordLink;
          if (cLink) {
            url = cLink[0].Url.replace('#?', '');   
          }
          if (url !== '') {
            recordLink = '<a title="Go to  record for ' + soR + '" href="' + url + '">';
          }
          else if (a.Header.DbId === 'nlebk')  {
            recordLink = '<a title="Go to OneSearch record for ' + soR + '" href="http://0-search.ebscohost.com.lasiii.losrios.edu/login.aspx?authtype=ip,guest&amp;custid=sacram&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;db=' + a.Header.DbId + '&amp;AN=' + a.Header.An + '&amp;site=eds-live&amp;scope=site">';
          }
          else {
            recordLink = '<a title="Go to OneSearch record for ' + soR + '" href="http://' + catDomain + '/login.aspx?authtype=ip,guest&amp;custid=sacram&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;bquery=AN+' + a.Header.An + '&amp;site=eds-live&amp;scope=site">';
          }
          
          output += recordLink;
          var img = a.ImageInfo;
          if (img)
          {
            output += '<img alt="' + title + '" src="' + a.ImageInfo[1].Target + '"></a>';
          }
          else
          {
            // title = title.replace(/ \/.*/, '');
            listItem.addClass('no-cover');
            output += title;
          }
          var itemData = a.Items;
          for (var j = 0; j < itemData.length; j++)
          {
            if (itemData[j].Group === 'TypPub')
            {
              //           console.log(itemData[j].Data);
              if (/video/i.test(itemData[j].Data) === true)
              {
                listItem.addClass('video');
              }
            }
          }
          /*       
        var pubType = a.Header.PubType;
        //     console.log(pubType);
        
        if (pubType.indexOf('Video') > -1)
        {
          listItem.addClass('video');
        }
        */
          //     console.log(output);
          listItem.html(output).appendTo(list);
        }
        var insertList = function() {
          setTimeout(function() {
            list.hide().prependTo(listArea).attr('id', 'booklist').fadeIn();
            butImg.attr('src', 'http://www.scc.losrios.edu/library/files/2016/10/refresh-icon-red.png').show();
        }, 500);
         };
        
        if ($('#booklist').length) {
        $('#booklist').fadeOut('fast', function() {
          $('#booklist').removeAttr('id').remove();
          insertList();
          });
        }
        else {
          insertList();
        }
        
        
        setTimeout(function ()
        {
          listArea.removeAttr('aria-live');
        }, 2000);
      }
      else
      {
        fallBackItems(listArea, butImg);
      }
      //   listArea.append('<button id="bl-refresh" onclick="rotateDisplay()">refresh</button>');
      //    list.html(output);
    }).fail(function (jqXHR, textStatus, errorThrown)
    {
      ga('localsiteTracker.send', 'event', gaCategory, 'error', 'EDS API call: ' + errorThrown);
      fallBackItems(listArea, butImg);
    });
  }
  // capture click events for items
  listArea.on('click', 'li a', function ()
  {
    ga('localsiteTracker.send', 'event', gaCategory, 'click', 'item');
  });
  $('#bl-refresh').on('click', function ()
  {
    ga('localsiteTracker.send', 'event', gaCategory, 'refresh');
  });
  $('.more-new-materials').on('click', function ()
  {
    ga('localsiteTracker.send', 'event', gaCategory, 'click', 'more');
  });
});