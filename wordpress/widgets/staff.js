jQuery(document).ready(function ($)
{
  var filtBut = $('#filter-buttons');
  filtBut.find('button').button();
  var url = 'http://www.scc.losrios.edu/faculty-staff-directory/?sortorder=lastfirst-name&firstrun=false&viewtype=table-view&alphasort=false&fname=&lname=&dept=&divsn=LR&submit=Search';
  $.get(url).done(function (data)
  {
    var html = $($.parseHTML(data));
    var table = html.find('.fsdirectory-table');
    table.addClass('orig-table');
    table.find('tbody tr').each(function ()
    {
      var a = $(this);
      if (/Librar|Flash|Murillo/.test(a.text()) === false)
      {
        a.remove();
      }
      var lastCell = a.find('td:last-of-type');
      lastCell.html(lastCell.html().replace('">http://', '">'));
    });
    table.find('th:first-child, td:first-child').remove();
    table.find('th').each(function ()
    {
      var a = $(this);
      a.html(a.text());
    });
    table.find('caption').html('Directory of Librarians, Library Instructors, Library Technicians and Administration');
    var chair = 'Karlsen';
    var chairRow = table.find('tr:contains("' + chair + '")');
    chairRow.html(chairRow.html().replace('Librarian', 'Librarian / Department Chair'));
    $('#loader').remove();
    $('#lrc-staff').append(table);
    filtBut.addClass('filters-displayed').show();
    filtBut.find('button').on('click', function ()
    {
      var butText = $(this).text();
      ga('localsiteTracker.send', 'event', 'staff page', 'filter', butText);
      var librarians = /bimbi|boyd|chen|clark|goodchi|harker|howe|karlse|livas|lopez|patrice|posz|terch|watts|woolle|yanez/i;
      var libTech = /baler|brothert|chekmar|cross|gage|george|izzo|nguy|reyes|shutak/i;
      var librLibt = /boyd|cello|goodchi|gordon|howe|lopez|posz|rowland|terch|xiaoli/i;
      var folks;
      var all = $('#show-all');
      var butID = $(this).attr('id');
      butTextHash = butText.toLowerCase().replace(/ &| \//g, '').replace(/ /g, '-');
      if (butID === 'show-all')
      {
        $('.filtered').remove();
        $('.orig-table').show();
        all.hide();
        butTextHash = 'all';
      }
      else
      {
        all.show();
        switch (butID)
        {
        case 'librarians':
          folks = librarians;
          break;
        case 'lib-tech':
          folks = libTech;
          break;
        case 'libr-libt':
          folks = librLibt;
          break;
        }
        $('.fsdirectory-table.filtered').remove();
        var filteredTable = table.clone().removeClass('orig-table').addClass('filtered');
        filteredTable.find('caption').html('Directory of ' + butText);
        var row = filteredTable.find('tbody tr');
        row.each(function ()
        {
          if (folks.test($(this).text()) === false)
          {
            $(this).remove();
          }
          $('.orig-table').hide();
          filteredTable.appendTo($('#lrc-staff')).show();
        });
 //       $(this).addClass('active');
      }
      history.pushState(null, null, '#' + butTextHash);
    });
    hashFilter();
    $(window).on('hashchange', hashFilter);
  }).fail(function (flim, flam, flum)
  {
    $('#loader').remove();
    ga('localsiteTracker.send', 'event', 'staff page', 'error', flum);
  });
});

function hashFilter()
{
  var $ = jQuery;
  if (location.hash === '#librarians')
  {
    $('#librarians').click().focus();
  }
  else if (location.hash === '#library-media-technical-assistants')
  {
    $('#lib-tech').click().focus();
  }
  else if (location.hash === '#libr-libt-instructors')
  {
    $('#libr-libt').click().focus();
  }
  else if (location.hash === '#all')
  {
    $('#show-all').click();
  }
}